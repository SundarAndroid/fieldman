
package com.alberichsoft.common.msg;

import com.alberichsoft.common.utility.CommonUtils;

import de.greenrobot.event.EventBus;

/**
 * Created by santhanasamy_a on 5/16/2017.
 */

public abstract class IMessage {

    public static final String TAG = IMessage.class.getSimpleName();

    protected final String eventName;

    protected IMessage(String aMsg) {
        eventName = aMsg;
    }

    public String getEventName() {
        return eventName;
    }

    public void post() {
        CommonUtils.printDLog(TAG, getEventName());
        EventBus.getDefault().post(this);
    }

    public void postSticky() {
        CommonUtils.printDLog(TAG, getEventName());
        EventBus.getDefault().postSticky(this);
    }

}
