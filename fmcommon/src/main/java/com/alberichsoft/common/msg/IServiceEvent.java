
package com.alberichsoft.common.msg;

import java.io.Serializable;

/**
 * 
 */
public class IServiceEvent extends IMessage implements Serializable {

    private final int eventID;

    private final int subEventID;

    private final Object object;

    private IServiceEvent(Builder aBuilder) {

        super(aBuilder.eventName);
        eventID = aBuilder.eventID;
        object = aBuilder.object;
        subEventID = aBuilder.subEventID;
    }

    public int getEventID() {
        return eventID;
    }

    public int getSubEventID() {
        return subEventID;
    }

    public Object getEventObject() {
        return object;
    }

    public static final class Builder {

        private int subEventID = -1;

        private int eventID = -1;

        private String eventName = "";

        private Object object = null;

        public Builder setEventName( String eventName ) {
            this.eventName = eventName;
            return this;
        }

        public Builder setEventID( int eventID ) {
            this.eventID = eventID;
            return this;
        }

        public Builder setSubEventID( int subEvent ) {
            this.subEventID = subEvent;
            return this;
        }

        public Builder setEventObject( Object object ) {
            this.object = object;
            return this;
        }

        public IMessage build() {
            return new IServiceEvent(this);
        }
    }
}
