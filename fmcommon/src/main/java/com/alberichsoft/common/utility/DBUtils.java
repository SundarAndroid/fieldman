
package com.alberichsoft.common.utility;

/**
 * Created by santhanasamy_a on 7/8/2017.
 */

public class DBUtils {
    public interface Table {

        String USERS = "users";

        String CUSTOMER = "customer";

        String HISTORY = "history";

        String APPOINTMENT = "appointment";

    }

    public interface CustomerField extends UserField {
        String CUSTOMER_ID = "customerID";
    }

    public interface UserField {

        String USER_NAME = "userName";

        String USER_ID = "userID";

        String APPOINTMENTS = "appointments";

        String HISTORY = "history";

        String USER_IMAGE_URL = "imageUrl";

        String IS_VERIFIED_USER = "isVerifiedUser";

        String USER_ROLE = "role";
    }

    public interface AppointmentField {
        String CUSTOMER_ID = "customerID";

        String TECHNICIAN_ID = "technicianID";

        String LOCATION = "location";

        String CREATED_TIME = "createdTime";

        String ASSIGNED_TIME = "assignedTime";

        String START_TIME = "startedTime";

        String END_TIME = "endTime";

        String STATUS = "status";

    }

}
