
package com.alberichsoft.common.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 
 */
public class TypefaceCache {

    private TypefaceCache() {
    }

    private static final String TAG = TypefaceCache.class.getSimpleName();

    private static final Map<String, Typeface> CACHE = new HashMap<String, Typeface>();

    public static Typeface getTypeface( Context c, String assetPath ) {
        synchronized (CACHE) {
            if (!CACHE.containsKey(assetPath)) {
                try {
                    Typeface t = CommonUtils.getTypeface(c, assetPath);
                    CACHE.put(assetPath, t);
                } catch (ClassCastException e) {
                    Log
                            .i(
                                    TAG,
                                    "Could not get typeface '" + assetPath + "' because "
                                            + e.getMessage());
                    throw e;
                }
            }
            return CACHE.get(assetPath);
        }
    }

}
