
package com.alberichsoft.common.utility;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import com.alberichsoft.fmcommon.BuildConfig;

import java.util.Arrays;

/**
 * Created by santhanasamy_a on 10/17/2016.
 */

public class CommonUtils {

    private static final String TAG = CommonUtils.class.getSimpleName();

    public static final boolean IS_DEBUG = true;

    public static final boolean IS_INFO = true;

    public static final boolean IS_ERROR = true;


    public static final boolean VERBOSE_LOG = BuildConfig.DEBUG;

    public static final boolean DEBUG_LOG = BuildConfig.DEBUG;

    public static final boolean ERROR_LOG = DEBUG_LOG;

    public static final boolean EXCEPTION_LOG = VERBOSE_LOG;

    public static final boolean INFO_LOG = true;


    public final static void printDLog( String aTAG, String aMsg, String... aStr ) {
        if (!IS_DEBUG) {
            return;
        }
        if (null == aStr || aStr.length == 0) {
            Log.d(aTAG, "[" + aMsg + "]");
        } else {
            Log.d(aTAG, "[" + aMsg + "]" + Arrays.asList(aStr));
        }
    }

    public final static void printELog( String aTAG, String aMsg, String... aStr ) {

        if (!IS_ERROR) {
            return;
        }

        Log.e(aTAG, "[" + aMsg + "]" + Arrays.asList(aStr));
    }

    public final static void printExLog( String aTAG, String aMsg, Exception aExc ) {

        if (!IS_ERROR) {
            return;
        }

        Log.e(aTAG, "[" + aMsg + "][" + aExc.getMessage() + "]");

        if (IS_DEBUG) {
            Log.e(aTAG, "[" + aMsg + ": TRACE ]");
            aExc.printStackTrace();
        }
    }

    public final static void printILog( String aTAG, String aMsg, String... aStr ) {

        if (!IS_INFO) {
            return;
        }
        Log.i(aTAG, "[" + aMsg + "]" + Arrays.asList(aStr));
    }

    public final static void printSys( String aMsg, String... aStr ) {

        if (!IS_DEBUG) {
            return;
        }
        int length = aStr.length;

        if (length == 1) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "]");
        } else if (length == 2) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "," + aStr[1] + "]");
        } else if (length == 3) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "," + aStr[1] + "," + aStr[2] + "]");
        } else {
            System.out.println("[" + aMsg + "]" + Arrays.asList(aStr));
        }
    }

    public final static void printException( String aTAG, Exception aException ) {

        if (null == aException) {
            return;
        }
        Log.e(aTAG, "[Error]" + aException.getMessage());

        if (!EXCEPTION_LOG) {
            return;
        }

        printStackTrace(aException, aTAG);

    }

    public final static void printException( String aTAG, String aMsg, Exception aException ) {

        if (null == aException) {
            return;
        }
        Log.e(aTAG, "[Error-Msg, Exception - Msg] [" + aMsg + ", " + aException.getMessage());

        if (!EXCEPTION_LOG) {
            return;
        }

        printStackTrace(aException, aTAG);

    }

    public static void printStackTrace( Exception e, String className ) {
        if (DEBUG_LOG) {
            e.printStackTrace();
        } else {
            Log.e(className, "" + e.getMessage());
        }
    }

    /**
     * Method to get the request Font from the assert folder. If their is no
     * font with the requested name default font will be return to the caller.
     *
     * @param context Context
     * @return Typeface
     */
    public static Typeface getTypeface(Context context, String aTypeFaceName ) {
        try {
            String fontPath = "fonts/" + aTypeFaceName;
            return Typeface.createFromAsset(context.getAssets(), fontPath);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return Typeface.DEFAULT;
        }
    }
}
