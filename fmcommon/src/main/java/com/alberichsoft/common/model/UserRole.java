
package com.alberichsoft.common.model;

import com.alberichsoft.fmcommon.R;

/**
 * 
 */
public enum UserRole {

    ADMIN(R.string.str_admin, R.drawable.ic_admin_small), MANAGER(R.string.str_manager,
            R.drawable.ic_manager_small), TECHNICIAN(R.string.str_technician,
                    R.drawable.ic_technician_small);

    int mName;

    int mRoleIcon;

    UserRole(int aNameString, int aRoleIcon) {
        mName = aNameString;
        mRoleIcon = aRoleIcon;
    }

    public static int getRoleName( int aRoleID ) {

        for (UserRole aRole : UserRole.values()) {

            if (aRoleID == aRole.ordinal()) {
                return aRole.getRoleString();
            }
        }
        return UserRole.TECHNICIAN.getRoleString();
    }

    public int getRoleString() {
        return mName;
    }

    public int getRoleIcon() {
        return mRoleIcon;
    }

}
