
package com.alberichsoft.common.model;

/**
 * Created by santhanasamy_a on 7/13/2017.
 */

public class Tuple<Elder, Younger> {
    private Elder elder;

    private Younger younger;

    public Elder getElder() {
        return elder;
    }

    public void setElder( Elder elder ) {
        this.elder = elder;
    }

    public Younger getYounger() {
        return younger;
    }

    public void setYounger( Younger younger ) {
        this.younger = younger;
    }
}
