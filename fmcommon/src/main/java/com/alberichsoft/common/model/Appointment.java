
package com.alberichsoft.common.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 *
 */
public class Appointment implements Parcelable {

    private String appointmentID = "";

    private String customerID = "";

    private String technicianID = "";

    private Location location = null;

    private String createdTime = "";

    private String assignedTime = "";

    private String startedTime = "";

    private String endTime = "";

    private String comment = "";

    private int status = AppointmentStatus.OPEN.ordinal();

    @Exclude
    private Customer customer = null;

    public Appointment() {

    }

    protected Appointment(Parcel in) {
        appointmentID = in.readString();
        customerID = in.readString();
        technicianID = in.readString();
        location = in.readParcelable(Location.class.getClassLoader());
        createdTime = in.readString();
        assignedTime = in.readString();
        startedTime = in.readString();
        endTime = in.readString();
        status = in.readInt();
        customer = in.readParcelable(Customer.class.getClassLoader());
        comment = in.readString();
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString(appointmentID);
        dest.writeString(customerID);
        dest.writeString(technicianID);
        dest.writeParcelable(location, flags);
        dest.writeString(createdTime);
        dest.writeString(assignedTime);
        dest.writeString(startedTime);
        dest.writeString(endTime);
        dest.writeInt(status);
         dest.writeParcelable(customer, flags);
        dest.writeString(comment);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Appointment> CREATOR = new Creator<Appointment>() {
        @Override
        public Appointment createFromParcel( Parcel in ) {
            return new Appointment(in);
        }

        @Override
        public Appointment[] newArray( int size ) {
            return new Appointment[size];
        }
    };

    public void setCustomerID( String aCustomerID ) {
        customerID = aCustomerID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setTechnicianID( String aTechnicianID ) {
        technicianID = aTechnicianID;
    }

    public String getTechnicianID() {
        return technicianID;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation( Location location ) {
        this.location = location;
    }

    public String getStartedTime() {
        return startedTime;
    }

    public void setStartedTime( String startedTime ) {
        this.startedTime = startedTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime( String endTime ) {
        this.endTime = endTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus( int appointmentStatus ) {
        this.status = appointmentStatus;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime( String createdTime ) {
        this.createdTime = createdTime;
    }

    public String getAssignedTime() {
        return assignedTime;
    }

    public void setAssignedTime( String assignedTime ) {
        this.assignedTime = assignedTime;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer( Customer customer ) {
        this.customer = customer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment( String comment ) {
        this.comment = comment;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID( String appointmentID ) {
        this.appointmentID = appointmentID;
    }

    @Override
    public String toString() {
        return "Appointment{" + "appointmentID='" + appointmentID + '\'' + ", customerID='"
                + customerID + '\'' + ", technicianID='" + technicianID + '\'' + ", location="
                + location + ", createdTime='" + createdTime + '\'' + ", assignedTime='"
                + assignedTime + '\'' + ", startedTime='" + startedTime + '\'' + ", endTime='"
                + endTime + '\'' + ", comment='" + comment + '\'' + ", status=" + status
                + ", customer=" + customer + '}';
    }
}
