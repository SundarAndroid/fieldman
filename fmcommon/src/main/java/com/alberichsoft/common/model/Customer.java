
package com.alberichsoft.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 */
public class Customer implements Parcelable {

    private String customerID = "";

    private String userName = "";

    private String email = "";

    private String photoUrl = "";

    private String mobileNumber = "";

    private String mAddress = "";

    private boolean isDeleted = false;

    public Customer() {

    }

    protected Customer(Parcel in) {
        customerID = in.readString();
        isDeleted = in.readByte() != 0;
        photoUrl = in.readString();
        userName = in.readString();
        email = in.readString();
        mobileNumber = in.readString();
        mAddress = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString(customerID);
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(photoUrl);
        dest.writeString(userName);
        dest.writeString(email);
        dest.writeString(mobileNumber);
        dest.writeString(mAddress);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName( String mUserName ) {
        this.userName = mUserName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String aEmail ) {
        this.email = aEmail;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl( String aUserImageURI ) {
        this.photoUrl = aUserImageURI;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID( String aUserID ) {
        this.customerID = aUserID;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted( boolean aIsDeleted ) {
        isDeleted = aIsDeleted;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber( String aMobileNumber ) {
        mobileNumber = aMobileNumber;
    }

    public void setAddress( String address ) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Customer{" + "customerID='" + customerID + '\'' + ", userName='" + userName + '\''
                + ", email='" + email + '\'' + ", photoUrl='" + photoUrl + '\'' + ", mobileNumber='"
                + mobileNumber + '\'' + ", isDeleted=" + isDeleted + '}';
    }

    public void fill( Customer aCustomer ) {
        customerID = aCustomer.customerID;
        photoUrl = aCustomer.photoUrl;
        userName = aCustomer.userName;
        email = aCustomer.email;
        mobileNumber = aCustomer.mobileNumber;
        mAddress = aCustomer.getAddress();
    }

}
