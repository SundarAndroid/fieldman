
package com.alberichsoft.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by santhanasamy_a on 11/6/2016.
 */

public class History implements Parcelable {

    private long from;

    private long to;

    private String device;

    private String user;

    private int deviceStatusWhileAssign;

    private int deviceStatusWhileReturn;

    public History() {

    }

    protected History(Parcel in) {
        from = in.readLong();
        to = in.readLong();
        device = in.readString();
        user = in.readString();
        deviceStatusWhileAssign = in.readInt();
        deviceStatusWhileReturn = in.readInt();
    }

    public static final Creator<History> CREATOR = new Creator<History>() {
        @Override
        public History createFromParcel( Parcel in ) {
            return new History(in);
        }

        @Override
        public History[] newArray( int size ) {
            return new History[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeLong(from);
        dest.writeLong(to);
        dest.writeString(device);
        dest.writeString(user);
        dest.writeInt(deviceStatusWhileAssign);
        dest.writeInt(deviceStatusWhileReturn);
    }

    @Override
    public String toString() {
        return "History{" + "from='" + from + '\'' + ", to='" + to + '\'' + ", device='" + device
                + '\'' + ", user='" + user + '\'' + ", deviceStatusWhileAssign='"
                + deviceStatusWhileAssign + '\'' + ", deviceStatusWhileReturn='"
                + deviceStatusWhileReturn + '\'' + '}';
    }

    public long getFrom() {
        return from;
    }

    public void setFrom( long from ) {
        this.from = from;
    }

    public long getTo() {
        return to;
    }

    public void setTo( long to ) {
        this.to = to;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice( String device ) {
        this.device = device;
    }

    public String getUser() {
        return user;
    }

    public void setUser( String user ) {
        this.user = user;
    }

    public int getDeviceStatusWhileAssign() {
        return deviceStatusWhileAssign;
    }

    public void setDeviceStatusWhileAssign( int deviceStatusWhileAssign ) {
        this.deviceStatusWhileAssign = deviceStatusWhileAssign;
    }

    public int getDeviceStatusWhileReturn() {
        return deviceStatusWhileReturn;
    }

    public void setDeviceStatusWhileReturn( int deviceStatusWhileReturn ) {
        this.deviceStatusWhileReturn = deviceStatusWhileReturn;
    }
}
