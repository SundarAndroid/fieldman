
package com.alberichsoft.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by santhanasamy_a on 10/23/2016.
 */

public class User implements Parcelable, Cloneable {

    private String userID;

    private String userName;

    private String password;

    private String email;

    private String imageUrl;

    private String role;

    private boolean isVerified = false;

    private boolean isDeleted = false;;

    public User() {

    }

    protected User(Parcel in) {
        userID = in.readString();
        role = in.readString();
        isVerified = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        imageUrl = in.readString();
        userName = in.readString();
        email = in.readString();
        // deviceList = in.createTypedArrayList(Device.CREATOR);
        // int size = in.readInt();
        // device = new HashMap<String, Device>(size);
        // for (int i = 0; i < size; i++) {
        // String key = in.readString();
        // Device value = in.readParcelable(Device.class.getClassLoader());
        // device.put(key, value);
        // }
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString(userID);
        dest.writeString(role);
        dest.writeByte((byte) (isVerified ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(imageUrl);
        dest.writeString(userName);
        dest.writeString(email);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel( Parcel in ) {
            return new User(in);
        }

        @Override
        public User[] newArray( int size ) {
            return new User[size];
        }
    };

    public String getUserName() {
        return userName;
    }

    public void setUserName( String mUserName ) {
        this.userName = mUserName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String aEmail ) {
        this.email = aEmail;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String aUserImageURI ) {
        this.imageUrl = aUserImageURI;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID( String aUserID ) {
        this.userID = aUserID;
    }

    public String getRole() {
        return role;
    }

    public void setRole( String aRole ) {
        this.role = aRole;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified( boolean aIsApproved ) {
        this.isVerified = aIsApproved;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted( boolean aIsDeleted ) {
        isDeleted = aIsDeleted;
    }

    // Don't use it to store password. Just a place holder for POJO object.
    // @Ignore
    public void setPassword( String aPassword ) {
        password = aPassword;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {

        return "[ Name,ID ,Role,Email,Device,Is approved?, IS deleted? ][" + userName + "," + userID
                + "," + role + "," + email + "," /* + device */ + "," + isVerified + "," + isDeleted
                + "]";
    }

    public void fill( User aUser ) {
        userID = aUser.userID;
        role = aUser.role;
        isVerified = aUser.isVerified;
        imageUrl = aUser.imageUrl;
        userName = aUser.userName;
        email = aUser.email;
    }
}
