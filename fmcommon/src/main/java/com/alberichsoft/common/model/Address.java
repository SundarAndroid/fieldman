
package com.alberichsoft.common.model;

/**
 * Created by santhanasamy_a on 7/7/2017.
 */

public class Address {

    private String lastName;

    private String phone;

    private String nickName;

    private String zipcode;

    private String state;

    private String address1;

    private String address2;

    private String firstName;

    private String addressId;

    private String city;

    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone( String phone ) {
        this.phone = phone;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName( String nickName ) {
        this.nickName = nickName;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode( String zipcode ) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState( String state ) {
        this.state = state;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1( String address1 ) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2( String address2 ) {
        this.address2 = address2;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId( String addressId ) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity( String city ) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" + "lastName='" + lastName + '\'' + ", phone='" + phone + '\''
                + ", nickName='" + nickName + '\'' + ", zipcode='" + zipcode + '\'' + ", state='"
                + state + '\'' + ", address1='" + address1 + '\'' + ", address2='" + address2 + '\''
                + ", firstName='" + firstName + '\'' + ", addressId='" + addressId + '\''
                + ", city='" + city + '\'' + '}';
    }
}
