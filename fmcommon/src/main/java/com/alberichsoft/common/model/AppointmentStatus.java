
package com.alberichsoft.common.model;

/**
 * Created by santhanasamy_a on 7/16/2017.
 */

public enum AppointmentStatus {
    OPEN, ASSIGNED, IN_PROGRESS, COMPLETED, MISSED, REJECTED
}
