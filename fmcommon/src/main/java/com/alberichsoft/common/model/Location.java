
package com.alberichsoft.common.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 */
public class Location implements Parcelable {

    private double mLatitude;

    private double mLongitude;

    public Location() {

    }

    public Location(double aLatitude, double aLongitude) {
        mLatitude = aLatitude;
        mLongitude = aLongitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

    protected Location(Parcel in) {
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel( Parcel in ) {
            return new Location(in);
        }

        @Override
        public Location[] newArray( int size ) {
            return new Location[size];
        }
    };

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude( double latitude ) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude( double longitude ) {
        this.mLongitude = longitude;
    }

}
