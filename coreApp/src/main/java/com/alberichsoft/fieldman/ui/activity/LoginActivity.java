
package com.alberichsoft.fieldman.ui.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.utility.CommonUtils;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.helper.LoginHelper;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;
import com.alberichsoft.fieldman.utility.UiUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;

import static com.alberichsoft.common.msg.EventID.ILogin;

public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private Button mLoginButton = null;

    private Button mRegisterButton = null;

    private EditText mUsernameView = null;

    private EditText mPasswordView = null;

    private TextView mWarningTxtView = null;

    private Dialog mProgressView = null;

    // String un,pwd;
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
    }

    @Override
    protected void initViews() {
        mRegisterButton = (Button) findViewById(R.id.btn_signup);
        mLoginButton = (Button) findViewById(R.id.btn_signin);
        mUsernameView = (EditText) findViewById(R.id.username_edit_text);
        mPasswordView = (EditText) findViewById(R.id.password_edit_text);
        mWarningTxtView = (TextView) findViewById(R.id.warning_txt);
        mWarningTxtView.setVisibility(View.GONE);

        mProgressView = UiUtils.getProgressDialog(
                this,
                getString(R.string.war_please_wait),
                getString(R.string.war_please_wait));

        mRegisterButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
    }

    public void onEvent( IEvent aEvent ) {

        showProgress(false);
        final int lID = aEvent.getEventID();

        switch (lID) {
            case ILogin.SIGN_IN_SUCCESS: {
                User lUser = (User) aEvent.getEventObject();
                handleLoginSuccess(lUser);
                break;
            }
            case ILogin.SIGN_IN_FAILED: {
                Toast.makeText(this, R.string.war_auth_failed, Toast.LENGTH_SHORT).show();
                Exception lException = (Exception) aEvent.getEventObject();
                handleLoginFailure(lException);
                break;
            }
            case ILogin.REQUEST_EMAIL_VERIFICATION: {
                Toast.makeText(this, R.string.war_email_verification, Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void attemptLogin() {

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString().trim();
        String password = mPasswordView.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!LoginHelper.isEmailValid(email)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            if (null == focusView) {
                focusView = mPasswordView;
            }
            cancel = true;
        } else if (!LoginHelper.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            if (null == focusView) {
                focusView = mPasswordView;
            }
            cancel = true;
        }

        if (cancel) {
            if (null != focusView) {
                focusView.requestFocus();
            }
        } else {
            showProgress(true);
            FBServiceUtils.getSigInEvent(email, password).post();
        }
    }

    private void handleLoginSuccess( User aUser ) {
        if (CommonUtils.DEBUG_LOG) {
            Log.d(TAG, "[LoginSuccess-Reason][" + aUser.getUserName() + "]");
        }
        LaunchUtils.startHomeActivity(this);
    }

    private void handleLoginFailure( Exception exception ) {

        if (null == exception) {
            return;
        }
        if (CommonUtils.DEBUG_LOG) {
            Log.d(TAG, "[LoginFailure-Reason][" + exception.getMessage() + "]");
        }

        mWarningTxtView.setText(exception.getMessage());
        mWarningTxtView.setVisibility(View.VISIBLE);

        Toast.makeText(this, R.string.war_auth_failed, Toast.LENGTH_LONG).show();

        mUsernameView.requestFocus();
    }

    @Override
    public void onClick( View v ) {

        int lID = v.getId();

        if (lID == R.id.btn_signup) {
            LaunchUtils.showSignupActivity(this);
        } else if (lID == R.id.btn_signin) {
            attemptLogin();
        }
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return false;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    protected void configureActionBar() {
    }

}
