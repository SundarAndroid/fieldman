
package com.alberichsoft.fieldman.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IServiceEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.viewholder.TechnicianViewHolder;
import com.alberichsoft.fieldman.utility.LaunchUtils;

import static com.alberichsoft.common.msg.EventID.IUser;
import static com.alberichsoft.fieldman.ui.base.PickerActivity.PickerMode;

/**
 * Created by santhanasamy_a on 7/10/2017.
 */

public class TechnicianAdapter extends BaseAdapter<User, TechnicianViewHolder>
        implements View.OnClickListener {

    private PickerMode mLaunchMode = null;

    public TechnicianAdapter(Context aContext, PickerMode aLaunchMode) {
        super(aContext);
        mLaunchMode = aLaunchMode;
    }

    @Override
    public void loadData() {

        new IServiceEvent.Builder()
                .setEventID(IUser.GET_TECHNICIAN_LIST)
                .setEventName("Get All Technicians from FB Remote")
                .build()
                .post();
    }

    @Override
    public TechnicianViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        View lView = LayoutInflater.from(mContext).inflate(R.layout.technician_list_item, null);
        return new TechnicianViewHolder(lView);
    }

    @Override
    public void onBindViewHolder( TechnicianViewHolder aViewHolder, int aPosition ) {
        User lUser = mDisplayedDataList.get(aPosition);
        if (null == aViewHolder || null == lUser) {
            return;
        }

        aViewHolder.getRootView().setOnClickListener(this);
        aViewHolder.getRootView().setTag(aPosition);
        aViewHolder.bindTechnicianInfo(lUser, mLaunchMode, false);

    }

    @Override
    public void onClick( View aView ) {
        int lPosition = (int) aView.getTag();
        LaunchUtils
                .sendTechnicianPickerResult((Activity) mContext, mDisplayedDataList.get(lPosition));
    }

}
