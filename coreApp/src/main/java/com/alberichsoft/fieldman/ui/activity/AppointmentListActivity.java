
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.adapter.AppointmentAdapter;
import com.alberichsoft.fieldman.ui.base.PickerActivity;
import com.alberichsoft.fieldman.ui.custom.EmptyWarningView;
import com.alberichsoft.fieldman.ui.decoration.DividerDecoration;
import com.alberichsoft.fieldman.utility.UiUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;

import java.util.List;

import static com.alberichsoft.common.msg.EventID.IAppointment;

/**
 * Created by santhanasamy_a on 7/16/2017.
 */

public class AppointmentListActivity extends PickerActivity {

    public static final int REQUEST_CODE_LAUCH_DETAIL = 100;

    private RecyclerView mTaskRecyclerView = null;

    private EmptyWarningView mEmptyWarningView = null;

    private Spinner mTaskTypeSpinnerView = null;

    private Spinner mTaskDateSpinnerView = null;

    private View mFilterContainer = null;

    private ArrayAdapter<String> mDateAdapter = null;

    private ArrayAdapter<String> mTaskTypeAdapter = null;

    protected AppointmentAdapter mTaskAdapter = null;

    protected List<Appointment> mAppointmentList = null;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_list);
    }

    @Override
    public void configureActionbarForMode( PickerMode aPickerMode ) {
        setActionBarTitle(mTitleStringID);
        setRightActionText(mActionStringID);
    }

    @Override
    protected void initViews() {

        mTaskTypeSpinnerView = (Spinner) findViewById(R.id.filter_task_type_spinner_view);
        mFilterContainer = findViewById(R.id.filter_container);

        mEmptyWarningView = (EmptyWarningView) findViewById(R.id.empty_warning_view);
        mEmptyWarningView.setWarningText(getString(R.string.war_no_appointment));
        updateEmptyViewState(false);

        mTaskTypeAdapter = new ArrayAdapter<String>(
                this,
                R.layout.simple_spinner_dropdown_item,
                R.id.task_type_txt_view,
                getResources().getStringArray(R.array.str_task_type_array));
        mTaskTypeSpinnerView.setAdapter(mTaskTypeAdapter);

        mTaskDateSpinnerView = (Spinner) findViewById(R.id.filter_task_date_spinner_view);

        mDateAdapter = new ArrayAdapter<String>(
                this,
                R.layout.simple_spinner_dropdown_item,
                R.id.task_type_txt_view,
                UiUtils.getTaskDates());
        mTaskDateSpinnerView.setAdapter(mDateAdapter);

        mTaskRecyclerView = (RecyclerView) findViewById(R.id.task_list_view);
        mTaskRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerDecoration itemDecoration = new DividerDecoration(
                this,
                R.dimen.task_list_item_divider_offset);
        mTaskRecyclerView.addItemDecoration(itemDecoration);

        mTaskAdapter = getAdapter();
        mTaskRecyclerView.setAdapter(mTaskAdapter);

        mFProgressView = UiUtils.getDialog(
                this,
                R.string.war_loading_appointment,
                R.drawable.ic_assign_appointment,
                false);
        mFProgressView.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterListener();
    }

    @Override
    public void onClick( View aView ) {
        super.onClick(aView);
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lEventID = aEvent.getEventID();

        if (IAppointment.GET_ALL_APPOINTMENTS_SUCCESS == lEventID) {

            mAppointmentList = (List<Appointment>) aEvent.getEventObject();
            mTaskAdapter.setData(mAppointmentList);

            updateEmptyViewState(null != mAppointmentList && !mAppointmentList.isEmpty());
            mFProgressView.dismiss();
        } else if (IAppointment.GET_ALL_APPOINTMENTS_FAILED == lEventID) {
            mFProgressView.dismiss();
            updateEmptyViewState(false);
            Toast.makeText(this, "Failed to get appointments", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        //finish();
    }

    protected void registerListener() {
        FBServiceUtils.registerAllValueListener();
    }

    protected void unRegisterListener() {
        FBServiceUtils.unRegisterAllValueListener();
    }

    protected AppointmentAdapter getAdapter() {
        return new AppointmentAdapter(this);
    }

    protected void updateEmptyViewState( boolean hasData ) {
        mEmptyWarningView.setVisibility(hasData ? View.GONE : View.VISIBLE);
        mFilterContainer.setVisibility(hasData ? View.VISIBLE : View.GONE);
    }

    protected void setData( List<Appointment> aAppointmentList ) {
        mTaskAdapter.setData(aAppointmentList);
    }
}
