
package com.alberichsoft.fieldman.ui.activity;

import android.os.Bundle;

import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.base.BaseActivity;

/**
 * Created by santhanasamy_a on 7/9/2017.
 */

public class MyProfileActivity extends BaseActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        setActionBarTitleAndLeftOnly(R.string.dash_action_my_profile);
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

}
