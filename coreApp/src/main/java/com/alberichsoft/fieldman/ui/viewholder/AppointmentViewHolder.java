
package com.alberichsoft.fieldman.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.Customer;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.utility.UiUtils;

public class AppointmentViewHolder extends RecyclerView.ViewHolder {

    private final TextView userName;

    private final TextView mobileNoView;

    private final TextView emailView;

    private final TextView addressView;

    private final ImageView profileImageView;

    private final TextView assignStartDateView;

    private final TextView assignEndDateView;

    private final View statusView;

    private final View rootView;

    public AppointmentViewHolder(View itemView) {

        super(itemView);

        rootView = itemView;
        userName = (TextView) itemView.findViewById(R.id.username_txt_text);

        mobileNoView = (TextView) itemView.findViewById(R.id.mobile_text_view);

        emailView = (TextView) itemView.findViewById(R.id.email_text_view);

        addressView = (TextView) itemView.findViewById(R.id.address_text_view);

        profileImageView = (ImageView) itemView.findViewById(R.id.profile_image_view);

        assignStartDateView = (TextView) itemView.findViewById(R.id.assign_start_text_view);

        assignEndDateView = (TextView) itemView.findViewById(R.id.assign_end_text_view);

        statusView = itemView.findViewById(R.id.complaint_status_text_view);
    }

    public void bindAppointmentInfo( Context aContext, Appointment aAppointment, int aPosition ) {

        statusView.setBackgroundResource(UiUtils.getAppointmentStatusColor(aAppointment.getStatus()));

        if (aPosition % 2 == 0) {
            profileImageView.setImageResource(R.drawable.temp_profile_img_1);
        } else {
            profileImageView.setImageResource(R.drawable.temp_profile_img_2);
        }
        Customer lCustomer = aAppointment.getCustomer();
        if (null != lCustomer) {
            userName.setText(lCustomer.getUserName());
            mobileNoView.setText(lCustomer.getMobileNumber());
            emailView.setText(lCustomer.getEmail());
            addressView.setText(lCustomer.getAddress());
            assignStartDateView.setText(aAppointment.getStartedTime());
        }
    }

    public View getRootView() {
        return rootView;
    }
}
