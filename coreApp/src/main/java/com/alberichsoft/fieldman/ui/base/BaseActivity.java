
package com.alberichsoft.fieldman.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewParent;

import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.custom.ActionBarView;

import de.greenrobot.event.EventBus;

import static com.alberichsoft.fieldman.utility.UiUtils.FullScreenDialog;
import static com.alberichsoft.fieldman.utility.UiUtils.getProgressDialog;

/**
 * BaseActivity for all the activities present in the Petco application.
 */
public abstract class BaseActivity extends AppCompatActivity implements OnClickListener {

    private static String TAG = BaseActivity.class.getSimpleName();

    protected static boolean inBackground = false;

    protected abstract boolean isBackButtonNeeded();

    protected abstract int getActionBarLayoutID();

    protected abstract void initViews();

    protected Context mContext = null;

    protected Dialog mProgressView = null;

    protected FullScreenDialog mFProgressView = null;

    private ActionBarView mActionBarLayout;

    private int mActionBarViewID = -1;

    protected BaseActivity() {
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        mContext = this;
        EventBus.getDefault().register(this);
        configureActionBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void setContentView( @LayoutRes int layoutResID ) {
        super.setContentView(layoutResID);

        mProgressView = getProgressDialog(
                this,
                getString(R.string.war_title_alert),
                getString(R.string.war_please_wait));

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (inBackground) {
            inBackground = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent( IEvent aEvent ) {

    }

    @Override
    public void startActivity( Intent aIntent ) {
        super.startActivity(aIntent);
        overridePendingTransition(R.anim.slide_right_to_left, R.anim.slide_inv_right_to_left);
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        inBackground = true;
    }

    @Override
    public void onClick( View aView ) {

        int lID = aView.getId();
        if (R.id.action_btn_left == lID) {
            onLeftActionClicked(aView);
        } else if (R.id.action_txt_right == lID) {
            onRightActionClicked(aView);
        }
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onRightActionClicked( View aRightActionTxtView ) {

    }

    protected void onLeftActionClicked( View aRightActionTxtView ) {

        finish();
    }

    protected void configureActionBar() {

        ActionBar lActionBar = getSupportActionBar();
        if (null == lActionBar) {
            return;
        }

        mActionBarViewID = getActionBarLayoutID();

        if (-1 == mActionBarViewID) {
            mActionBarLayout = new ActionBarView(this);
        }

        if (-1 != mActionBarViewID) {
            lActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            lActionBar.setCustomView(mActionBarViewID);
            mActionBarLayout = (ActionBarView) lActionBar.getCustomView();
            if (null != mActionBarLayout && isBackButtonNeeded()) {
                mActionBarLayout.setLeftActionImage(R.drawable.ic_back_arrow_white);
            }
        } else if (null != mActionBarLayout) {
            lActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            lActionBar.setCustomView(mActionBarLayout);
            ViewParent lParent = mActionBarLayout.getParent();
            if (null != lParent && lParent instanceof Toolbar) {
                ((Toolbar) lParent).setContentInsetsAbsolute(0, 0);
            }
            if (isBackButtonNeeded()) {
                mActionBarLayout.setLeftActionImage(R.drawable.ic_back_arrow_white);
                mActionBarLayout.getLeftActionView().setOnClickListener(this);
            }
            mActionBarLayout.getRightActionView().setOnClickListener(this);
        } else {
            if (isBackButtonNeeded()) {
                lActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    public void setActionBarTitleAndLeftOnly( int aStrId ) {

        setActionBarTitle(aStrId);
        if (null != mActionBarLayout) {
            mActionBarLayout.enableLeftAndTitle();
        }
    }

    public void setActionBarTitle( int aStrId ) {
        setActionBarTitle(getString(aStrId));
    }

    public void setActionBarTitle( String aStr ) {

        ActionBar lActionBar = getSupportActionBar();
        if (null == lActionBar) {
            return;
        }

        if (-1 != mActionBarViewID) {
            ActionBarView lView = (ActionBarView) lActionBar.getCustomView();
            if (null != lView) {
                lView.setTitle(aStr);
            }
        } else if (null != mActionBarLayout) {
            mActionBarLayout.setTitle(aStr);
        } else {
            getSupportActionBar().setTitle(aStr);
        }
    }

    public void setActionBarTitleGravity( int textGravity ) {
        if (null != mActionBarLayout) {
            mActionBarLayout.setTextGravity(textGravity);
        }
    }

    public void setRightActionText( int aStrId ) {

        if (null == mActionBarLayout) {
            return;
        }
        mActionBarLayout.getRightActionView().setOnClickListener(this);

        if (-1 != aStrId) {
            mActionBarLayout.setRightActionTxt(getString(aStrId), isBackButtonNeeded());
        } else {
            mActionBarLayout.setRightActionTxt(null, isBackButtonNeeded());
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    protected void showProgress( final boolean show ) {

        if (null == mProgressView) {
            return;
        }
        if (show) {
            mProgressView.show();
        } else {
            mProgressView.dismiss();
        }
    }
}
