
package com.alberichsoft.fieldman.ui.activity;

import android.os.Bundle;

import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.base.BaseActivity;

/**
 * Created by santhanasamy_a on 7/9/2017.
 */

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setActionBarTitleAndLeftOnly(R.string.dash_action_settings);
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

}
