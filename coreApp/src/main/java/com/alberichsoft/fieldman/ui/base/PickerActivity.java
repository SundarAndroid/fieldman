
package com.alberichsoft.fieldman.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * BaseActivity for all the activities present in the Petco application.
 */
public abstract class PickerActivity extends BaseActivity {

    public static final String EXTRA_KEY_LAUNCH_MODE = "MapActivity_Launch_Mode";

    public static final String EXTRA_KEY_TITLE = "Title String";

    public static final String EXTRA_KEY_ACTION_BTN_STRING = "Action String";

    public static final String EXTRA_KEY_OBJECT = "Extra Object";

    public static final String EXTRA_KEY_OBJECT_1 = "Extra Object 1";

    public static final String BUNDLE_KEY_SINGLE_PICK_RESULT = "Single_Pick_Result";

    public static final String BUNDLE_KEY_MULTI_PICK_RESULT = "Multi_Pick_Result";

    public enum PickerMode {
        LIST, SINGLE_PICK, MULTI_PICK;

        public static PickerMode getMode( int ordinal ) {

            for (PickerMode lPickerMode : PickerMode.values()) {

                if (ordinal == lPickerMode.ordinal()) {
                    return lPickerMode;
                }
            }
            return PickerMode.LIST;
        }
    }

    protected PickerMode mLaunchPickerMode = PickerMode.LIST;

    protected boolean mIsPickerMode = false;

    protected int mTitleStringID = -1;

    protected int mActionStringID = -1;

    public abstract void configureActionbarForMode( PickerMode aPickerMode );

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        resolveLaunchModeInternal();
        configureActionbarForMode(mLaunchPickerMode);
    }

    private void resolveLaunchModeInternal() {
        Intent lIntent = getIntent();
        mLaunchPickerMode = PickerMode
                .getMode(lIntent.getIntExtra(EXTRA_KEY_LAUNCH_MODE, PickerMode.LIST.ordinal()));
        mIsPickerMode = (PickerMode.SINGLE_PICK == mLaunchPickerMode
                || PickerMode.MULTI_PICK == mLaunchPickerMode);
        mTitleStringID = lIntent.getIntExtra(EXTRA_KEY_TITLE, -1);
        mActionStringID = lIntent.getIntExtra(EXTRA_KEY_ACTION_BTN_STRING, -1);
    }

    public PickerMode getLaunchMode() {
        return mLaunchPickerMode;
    }

}
