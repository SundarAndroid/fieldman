
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.widget.Toast;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.adapter.AppointmentAdapter;
import com.alberichsoft.fieldman.ui.adapter.MyAppointmentAdapter;
import com.alberichsoft.fieldman.ui.base.PickerActivity;

import java.util.List;

import static com.alberichsoft.common.msg.EventID.IAppointment;

/**
 *
 * 
 */
public class MyAppointmentListActivity extends AppointmentListActivity {

    @Override
    public void configureActionbarForMode( PickerActivity.PickerMode aPickerMode ) {
        setActionBarTitleAndLeftOnly(R.string.dash_action_appointment);
    }

    @Override
    protected AppointmentAdapter getAdapter() {
        return new MyAppointmentAdapter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (null != mAppointmentList) {
            mTaskAdapter.loadData();
        }
    }

    @Override
    protected void registerListener() {
    }

    @Override
    protected void unRegisterListener() {

    }

    @Override
    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lEventID = aEvent.getEventID();

        if (IAppointment.GET_USER_APPOINTMENTS_SUCCESS == lEventID) {

            mAppointmentList = (List<Appointment>) aEvent.getEventObject();
            mTaskAdapter.setData(mAppointmentList);

            updateEmptyViewState(null != mAppointmentList && !mAppointmentList.isEmpty());
            mFProgressView.dismiss();
        } else if (IAppointment.GET_USER_APPOINTMENTS_FAILED == lEventID) {
            mFProgressView.dismiss();
            updateEmptyViewState(false);
            Toast.makeText(this, "Failed to get appointments", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
    }
}
