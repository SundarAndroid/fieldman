
package com.alberichsoft.fieldman.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alberichsoft.fieldman.R;

public class EmptyWarningView extends LinearLayout {

    private TextView mEmptyWarTxtView = null;

    private ImageView mEmptyWarImage = null;

    public EmptyWarningView(Context context) {
        this(context, null);
    }

    public EmptyWarningView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmptyWarningView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init( final Context context, final AttributeSet attrs ) {

        View lView = LayoutInflater.from(context).inflate(R.layout.empty_view, this, true);

        setOrientation(LinearLayout.VERTICAL);
        int lPadding = (int) getResources().getDimension(R.dimen.app_padding);
        setPadding(lPadding, lPadding, lPadding, lPadding);
        setGravity(Gravity.CENTER);

        // Set the values if provided in the layout XML
        if (attrs != null) {

            // Read styleable for empty view
            final TypedArray lStyleArray = context
                    .obtainStyledAttributes(attrs, R.styleable.EmptyWarningView);

            int lWarTextValue = lStyleArray
                    .getResourceId(R.styleable.EmptyWarningView_warning_txt, -1);

            mEmptyWarTxtView = (TextView) lView.findViewById(R.id.warning_txt);
            mEmptyWarImage = (ImageView) lView.findViewById(R.id.warning_image);

            int lImageWarID = lStyleArray
                    .getResourceId(R.styleable.EmptyWarningView_warning_image, -1);
            if (-1 != lImageWarID) {
                mEmptyWarImage.setImageResource(lImageWarID);
            } else {
                mEmptyWarImage.setImageResource(R.drawable.ic_empty);
            }

            if (-1 != lWarTextValue) {
                mEmptyWarTxtView.setText(getResources().getString(lWarTextValue));
            }
            // Recycle the styleable
            lStyleArray.recycle();
        }
    }

    public void setWarningText( String aWarningText ) {

        if (TextUtils.isEmpty(aWarningText)) {
            aWarningText = "";
        }
        mEmptyWarTxtView.setText(aWarningText);
    }

    public void setWarningImage( int aWarningImage ) {
        if (-1 != aWarningImage) {
            mEmptyWarImage.setImageResource(aWarningImage);
        }
    }

    public String getWarningText() {
        return mEmptyWarTxtView.getText().toString();
    }

}
