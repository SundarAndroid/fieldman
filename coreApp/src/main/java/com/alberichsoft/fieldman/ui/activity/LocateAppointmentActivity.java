
package com.alberichsoft.fieldman.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.AppointmentStatus;
import com.alberichsoft.common.model.Location;
import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.listeners.OnMapAndViewReadyListener;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.GlideImageLoader;
import com.alberichsoft.fieldman.utility.UiUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.alberichsoft.common.msg.EventID.IAppointment;
import static com.alberichsoft.common.msg.EventID.IUser;
import static com.alberichsoft.fieldman.listeners.OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener;

public class LocateAppointmentActivity extends BaseActivity
        implements GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerDragListener, SeekBar.OnSeekBarChangeListener,
        GoogleMap.OnInfoWindowLongClickListener, GoogleMap.OnInfoWindowCloseListener,
        OnGlobalLayoutAndMapReadyListener, View.OnClickListener {

    public static String EXTRA_KEY_LAUNCH_MODE = "EXTRA_KEY_LAUNCH_MODE";

    public static String EXTRA_KEY_APPOINTMENT_LIST = "EXTRA_KEY_APPOINTMENT_LIST";

    public static String EXTRA_KEY_TECHNICIAN_DETAIL = "EXTRA_KEY_TECHNICIAN_DETAIL";

    public static final String EXTRA_KEY_PICKER_RESULT = "Location";

    public enum MapActivityMode {

        PICK_LOCATION, SHOW_APPOINTMENT;

        public static MapActivityMode getMode( int aOrdinal ) {
            for (MapActivityMode aMode : MapActivityMode.values()) {

                if (aMode.ordinal() == aOrdinal) {
                    return aMode;
                }
            }
            return MapActivityMode.SHOW_APPOINTMENT;
        }
    }

    private MapActivityMode mode = MapActivityMode.SHOW_APPOINTMENT;

    private ImageView mFabProfileImgView = null;

    private ImageView mCalenderImgView = null;

    private ImageView mSettingsImgView = null;

    private ImageView mHistoryImgView = null;

    private SupportMapFragment mapFragment = null;

    private GoogleMap mGoogleMap = null;

    private List<Appointment> mAppointmentList = null;

    private Map<Integer, BitmapDescriptor> mDescriptorMarkerMap = null;

    private User mTechnician = null;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        initBitmapDescriptorForMapMarker();

        mode = MapActivityMode.getMode(getIntent().getIntExtra(EXTRA_KEY_LAUNCH_MODE, 0));
        if (mode == MapActivityMode.SHOW_APPOINTMENT) {
            setActionBarTitleAndLeftOnly(R.string.dash_action_locate_appointment);
        } else {
            setActionBarTitleAndLeftOnly(R.string.str_locate_customer);
        }
    }

    @Override
    protected void initViews() {

        mFProgressView = UiUtils.getDialog(
                this,
                R.string.war_loading_appointment,
                R.drawable.ic_assign_appointment,
                false);
        mFProgressView.show();

        Intent lIntent = getIntent();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        new OnMapAndViewReadyListener(mapFragment, this);

        if (mode == MapActivityMode.SHOW_APPOINTMENT) {
            mTechnician = lIntent.getParcelableExtra(EXTRA_KEY_TECHNICIAN_DETAIL);
            mAppointmentList = lIntent.getParcelableExtra(EXTRA_KEY_APPOINTMENT_LIST);

            if (null == mTechnician || null == mAppointmentList) {
                loadData();
            }
        }

        // Current User Location
        LocationManager myManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    @Override
    public void onMapReady( GoogleMap googleMap ) {

        mGoogleMap = googleMap;

        // Hide the zoom controls as the button panel will cover it.
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

        if (mode == MapActivityMode.PICK_LOCATION) {
            mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick( LatLng aLatLong ) {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(EXTRA_KEY_PICKER_RESULT, aLatLong);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
        } else if (mode == MapActivityMode.SHOW_APPOINTMENT) {
            configureMap();
        }
        initFloatingWindowForMap();
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    public void loadData() {
        FBServiceUtils.postGetCurrentUserAppointment();
        FBServiceUtils.postGetUserEvent();
    }

    private void configureMap() {

        if (null == mAppointmentList || mAppointmentList.isEmpty()) {
            return;
        }

        Marker lMarker;
        List<Marker> lMarkerList = new ArrayList<>();

        LatLngBounds.Builder lBuilder = new LatLngBounds.Builder();
        for (Appointment lAppointment : mAppointmentList) {

            Location lLocation = lAppointment.getLocation();
            LatLng latlng = new LatLng(lLocation.getLatitude(), lLocation.getLongitude());

            // Uses a colored icon.
            lMarker = mGoogleMap.addMarker(
                    new MarkerOptions()
                            .position(latlng)
                            .title(lAppointment.getCustomer().getUserName())
                            .snippet("Population: 2,074,200")
                            .icon(mDescriptorMarkerMap.get(lAppointment.getStatus()))
                            .infoWindowAnchor(0.5f, 0.5f)
                            .draggable(false));
            lMarkerList.add(lMarker);
            lBuilder.include(latlng);
        }

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(lBuilder.build(), 50));
    }

    /**
     * Called when the Clear button is clicked.
     */
    public void onClearMap( View view ) {
        if (!checkReady()) {
            return;
        }
        mGoogleMap.clear();
    }

    private boolean checkReady() {
        if (mGoogleMap == null) {
            Toast.makeText(this, R.string.war_map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick( View aView ) {
        super.onClick(aView);

        Object lObj = aView.getTag();
        if (null != lObj && lObj instanceof Integer) {

            int lViewID = Integer.parseInt(lObj.toString());
            if (R.drawable.ic_calender == lViewID) {

            } else if (R.drawable.ic_settings_white == lViewID) {

            } else if (R.drawable.ic_history == lViewID) {

            }
        }
    }

    @Override
    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lEventID = aEvent.getEventID();

        if (IAppointment.GET_USER_APPOINTMENTS_SUCCESS == lEventID) {
            mAppointmentList = (List<Appointment>) aEvent.getEventObject();
            mFProgressView.dismiss();
            configureMap();
        } else if (IAppointment.GET_USER_APPOINTMENTS_FAILED == lEventID) {
            mFProgressView.dismiss();
            Toast
                    .makeText(this, R.string.war_get_user_appointment_failed, Toast.LENGTH_SHORT)
                    .show();
        } else if (IUser.GET_CURRENT_USER_SUCCESS == lEventID) {
            mFProgressView.dismiss();
            mTechnician = (User) aEvent.getEventObject();

            if (null != mTechnician && null != mFabProfileImgView) {
                if (!TextUtils.isEmpty(mTechnician.getImageUrl())) {
                    GlideImageLoader.loadImage(
                            mFabProfileImgView,
                            mTechnician.getImageUrl(),
                            R.drawable.ic_user,
                            R.drawable.ic_user,
                            true);
                }
            }

        } else if (IUser.GET_CURRENT_USER_FAILED == lEventID) {
            mFProgressView.dismiss();
            Toast.makeText(this, R.string.war_get_user_failed, Toast.LENGTH_SHORT).show();
        }

    }

    public void initBitmapDescriptorForMapMarker() {

        mDescriptorMarkerMap = new HashMap<>();

        int lOpenColor = ContextCompat.getColor(this, R.color.task_status_open);
        int lAssignedColor = getResources().getColor(R.color.task_status_assigned);
        int lInProgressColor = getResources().getColor(R.color.task_status_in_progress);
        int lCompletedColor = getResources().getColor(R.color.task_status_completed);
        int lMissedColor = getResources().getColor(R.color.task_status_missed);
        int lRejectedColor = getResources().getColor(R.color.task_status_rejected);

        BitmapDescriptor lOpenColorDes = UiUtils.getMarkerDesIconFromColor("#62edfa");
        BitmapDescriptor lAssignedColorDes = UiUtils.getMarkerDesIconFromColor("#0000ff");

        BitmapDescriptor lInProgressColorDes = UiUtils.getMarkerDesIconFromColor("#FF6F00");
        BitmapDescriptor lCompletedColorDes = UiUtils.getMarkerDesIconFromColor("#006400");

        BitmapDescriptor lMissedColorDes = UiUtils.getMarkerDesIconFromColor("#FF0000");
        BitmapDescriptor lRejectedColorDes = UiUtils.getMarkerDesIconFromColor("#4d4d4d");

        mDescriptorMarkerMap.put(AppointmentStatus.OPEN.ordinal(), lOpenColorDes);
        mDescriptorMarkerMap.put(AppointmentStatus.ASSIGNED.ordinal(), lAssignedColorDes);
        mDescriptorMarkerMap.put(AppointmentStatus.IN_PROGRESS.ordinal(), lInProgressColorDes);
        mDescriptorMarkerMap.put(AppointmentStatus.COMPLETED.ordinal(), lCompletedColorDes);
        mDescriptorMarkerMap.put(AppointmentStatus.MISSED.ordinal(), lMissedColorDes);
        mDescriptorMarkerMap.put(AppointmentStatus.REJECTED.ordinal(), lRejectedColorDes);
    }

    // Map Call backs
    @Override
    public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {

    }

    @Override
    public void onStartTrackingTouch( SeekBar seekBar ) {

    }

    @Override
    public void onStopTrackingTouch( SeekBar seekBar ) {

    }

    @Override
    public void onInfoWindowClick( Marker marker ) {

    }

    @Override
    public void onInfoWindowClose( Marker marker ) {

    }

    @Override
    public void onInfoWindowLongClick( Marker marker ) {

    }

    @Override
    public boolean onMarkerClick( Marker marker ) {
        return false;
    }

    @Override
    public void onMarkerDragStart( Marker marker ) {

    }

    @Override
    public void onMarkerDrag( Marker marker ) {

    }

    @Override
    public void onMarkerDragEnd( Marker marker ) {

    }

    private void initFloatingWindowForMap() {

        if (null == mTechnician) {
            return;
        }

        Resources lRes = getResources();

        int lFabBtnSize = lRes.getDimensionPixelSize(R.dimen.fab_action_btn_size);
        int lFabBtnContentSize = lRes.getDimensionPixelSize(R.dimen.fab_action_btn_content_size);

        int lFabMargin = lRes.getDimensionPixelOffset(R.dimen.fab_action_btn_margin);
        int lFabBtnContentPadding = lRes
                .getDimensionPixelOffset(R.dimen.fab_action_btn_content_padding);

        int lFabActionBtnRadius = lRes.getDimensionPixelSize(R.dimen.fab_action_menu_radius);

        int lSubActionBtnSize = lRes.getDimensionPixelSize(R.dimen.s_action_button_size);
        int lSubActionBtnMargin = lRes.getDimensionPixelSize(R.dimen.s_action_button_margin);

        // 1. Create an icon
        mFabProfileImgView = new ImageView(this);
        mFabProfileImgView.setBackgroundResource(R.drawable.blue_circle_drawable);
        mFabProfileImgView.setImageResource(R.drawable.ic_user);
        mFabProfileImgView.setPadding(
                lFabBtnContentPadding,
                lFabBtnContentPadding,
                lFabBtnContentPadding,
                lFabBtnContentPadding);

        FloatingActionButton.LayoutParams lFabParams = new FloatingActionButton.LayoutParams(
                lFabBtnSize,
                lFabBtnSize);
        lFabParams.setMargins(lFabMargin, lFabMargin, lFabMargin, lFabMargin);

        FloatingActionButton.LayoutParams lContentParams = new FloatingActionButton.LayoutParams(
                lFabBtnContentSize,
                lFabBtnContentSize);
        lFabParams.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        lContentParams.setMargins(
                lFabBtnContentPadding,
                lFabBtnContentPadding,
                lFabBtnContentPadding,
                lFabBtnContentPadding);

        FloatingActionButton lFabActionButton = new FloatingActionButton.Builder(this)
                .setPosition(FloatingActionButton.POSITION_BOTTOM_RIGHT)
                .setContentView(mFabProfileImgView, lContentParams)
                .setBackgroundDrawable(R.drawable.map_sub_menu_selector)
                .setLayoutParams(lFabParams)
                .build();
        lFabActionButton.setBackgroundResource(android.R.color.transparent);

        if (!TextUtils.isEmpty(mTechnician.getImageUrl())) {
            GlideImageLoader.loadImage(
                    mFabProfileImgView,
                    mTechnician.getImageUrl(),
                    R.drawable.ic_user,
                    R.drawable.ic_user,
                    true);
        }

        // 2. Sub views
        FrameLayout.LayoutParams lSubLayoutParams = new FrameLayout.LayoutParams(
                lSubActionBtnSize,
                lSubActionBtnSize);
        lSubLayoutParams.setMargins(
                lSubActionBtnMargin,
                lSubActionBtnMargin,
                lSubActionBtnMargin,
                lSubActionBtnMargin);

        mCalenderImgView = new ImageView(this);
        mCalenderImgView.setImageResource(R.drawable.ic_calender);
        mCalenderImgView.setTag(R.drawable.ic_calender);
        mCalenderImgView.setOnClickListener(this);

        mSettingsImgView = new ImageView(this);
        mSettingsImgView.setImageResource(R.drawable.ic_settings_white);
        mSettingsImgView.setTag(R.drawable.ic_settings_white);
        mSettingsImgView.setOnClickListener(this);

        mHistoryImgView = new ImageView(this);
        mHistoryImgView.setImageResource(R.drawable.ic_history);
        mHistoryImgView.setTag(R.drawable.ic_history);
        mHistoryImgView.setOnClickListener(this);

        SubActionButton.Builder lCSubBuilder = new SubActionButton.Builder(this);
        lCSubBuilder
                .setBackgroundDrawable(lRes.getDrawable(R.drawable.map_sub_menu_selector))
                .setLayoutParams(lSubLayoutParams);

        // repeat many times:
        SubActionButton button1 = lCSubBuilder.setContentView(mCalenderImgView).build();
        SubActionButton button2 = lCSubBuilder.setContentView(mSettingsImgView).build();
        SubActionButton button3 = lCSubBuilder.setContentView(mHistoryImgView).build();

        View lView0 = lCSubBuilder.setContentView(button1, lSubLayoutParams).build();
        View lView1 = lCSubBuilder.setContentView(button2, lSubLayoutParams).build();
        View lView2 = lCSubBuilder.setContentView(button3, lSubLayoutParams).build();
        lView0.setBackgroundResource(android.R.color.transparent);
        lView1.setBackgroundResource(android.R.color.transparent);
        lView2.setBackgroundResource(android.R.color.transparent);

        // 3. Mapping.
        FloatingActionMenu actionMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(lView0)
                .addSubActionView(lView1)
                .addSubActionView(lView2)
                .setRadius(lFabActionBtnRadius)
                .attachTo(lFabActionButton)
                .build();

    }

}
