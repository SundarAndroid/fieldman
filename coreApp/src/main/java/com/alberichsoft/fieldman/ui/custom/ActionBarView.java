
package com.alberichsoft.fieldman.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alberichsoft.fieldman.R;

/**
 * Created by santhanasamy_a on 10/27/2016.
 */

public class ActionBarView extends RelativeLayout {

    private ImageView mLeftActionBtn = null;

    private TextView mRightActionTxtView = null;

    private TextView mTitleTxtView = null;

    private View mRightContainer = null;

    private int textGravity;

    public ActionBarView(Context context) {
        this(context, null);
    }

    public ActionBarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ActionBarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init( final Context context, final AttributeSet attrs ) {

        View lView = LayoutInflater
                .from(context)
                .inflate(R.layout.common_actionbar_layout, this, true);

        mLeftActionBtn = (ImageView) lView.findViewById(R.id.action_btn_left);

        mRightContainer = lView.findViewById(R.id.action_right_container);
        mRightActionTxtView = (TextView) lView.findViewById(R.id.action_txt_right);

        mTitleTxtView = (TextView) lView.findViewById(R.id.action_bar_title);
        mTitleTxtView.setGravity(Gravity.CENTER);

        // Set the values if provided in the layout XML
        if (attrs != null) {

            // Read styleable for empty view
            final TypedArray lStyleArray = context
                    .obtainStyledAttributes(attrs, R.styleable.NameValueTxtView);

            int lLeftActionImageID = lStyleArray
                    .getResourceId(R.styleable.ActionBarView_left_action_image, -1);
            int lRightActionImageID = lStyleArray
                    .getResourceId(R.styleable.ActionBarView_right_action_image, -1);

            int lTitleGravity = lStyleArray
                    .getResourceId(R.styleable.ActionBarView_title_gravity, -1);

            String lTitleString = lStyleArray.getString(R.styleable.ActionBarView_title_txt);

            if (-1 != lTitleGravity) {
                mTitleTxtView.setGravity(lTitleGravity);
            }
            if (-1 != lLeftActionImageID) {
                setLeftActionImage(lLeftActionImageID);
                setRightActionTxt(lRightActionImageID, true);
            } else {
                setRightActionTxt(lRightActionImageID, false);
            }
            setTitle(lTitleString);

            // Recycle the styleable
            lStyleArray.recycle();
        }
    }

    public void setTitle( int aStrID ) {
        setTitle(getResources().getString(aStrID));
    }

    public void setTitle( String name ) {

        if (!TextUtils.isEmpty(name)) {
            mTitleTxtView.setText(name);
            mTitleTxtView.setVisibility(View.VISIBLE);
        } else {
            mTitleTxtView.setVisibility(View.GONE);
        }
    }

    public void setLeftActionImage( int aImageID ) {

        if (-1 != aImageID) {
            mLeftActionBtn.setImageResource(aImageID);
            mLeftActionBtn.setVisibility(View.VISIBLE);
        } else {
            mLeftActionBtn.setVisibility(View.GONE);
        }
    }

    public View getLeftActionView() {
        return mLeftActionBtn;
    }

    public View getRightActionView() {
        return mRightActionTxtView;
    }

    public void setRightActionTxt( String aString, boolean aIsLeftActionVisible ) {

        if (!TextUtils.isEmpty(aString)) {
            mRightActionTxtView.setText(aString);
            mRightContainer.setVisibility(View.VISIBLE);
        } else {
            if (aIsLeftActionVisible) {
                mRightContainer.setVisibility(View.INVISIBLE);
            } else {
                mRightContainer.setVisibility(View.GONE);
            }
        }
    }

    public void setRightActionTxt( int aStringID, boolean aIsLeftActionVisible ) {

        if (-1 != aStringID) {

            setTextGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            mRightActionTxtView.setText(aStringID);
            mRightContainer.setVisibility(View.VISIBLE);
        } else {
            setTextGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            if (aIsLeftActionVisible) {
                mRightContainer.setVisibility(View.INVISIBLE);
            } else {
                mRightContainer.setVisibility(View.GONE);
            }
        }
    }

    public void setTextGravity( int textGravity ) {
        mTitleTxtView.setGravity(textGravity);
    }

    public void enableLeftAndTitle() {
        mTitleTxtView.setVisibility(VISIBLE);
        mLeftActionBtn.setVisibility(View.VISIBLE);
        mRightContainer.setVisibility(View.INVISIBLE);
    }
}
