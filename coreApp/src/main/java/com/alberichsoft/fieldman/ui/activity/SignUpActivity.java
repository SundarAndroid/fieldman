
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.utility.CommonUtils;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.helper.LoginHelper;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;

import static com.alberichsoft.common.msg.EventID.ILogin;

public class SignUpActivity extends BaseActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    private Button mLoginButton = null;

    private Button mRegisterButton = null;

    private EditText mDisplayNameView = null;

    private EditText mUsernameView = null;

    private EditText mPasswordView = null;

    private TextView mWarningTxtView = null;

    // String un,pwd;
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    protected void initViews() {
        mRegisterButton = (Button) findViewById(R.id.btn_signup);
        mLoginButton = (Button) findViewById(R.id.btn_signin);
        mDisplayNameView = (EditText) findViewById(R.id.display_name_edit_text);
        mUsernameView = (EditText) findViewById(R.id.username_edit_text);
        mPasswordView = (EditText) findViewById(R.id.password_edit_text);
        mWarningTxtView = (TextView) findViewById(R.id.warning_txt);
        mWarningTxtView.setVisibility(View.GONE);

        mRegisterButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
    }

    /**
     * Attempts to register the account specified by the login form. If there
     * are form errors (invalid email, missing fields, etc.), the errors are
     * presented and no actual login attempt is made.
     */

    private void attemptSignup() {

        // Reset errors.
        mDisplayNameView.setError(null);
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String displayName = mDisplayNameView.getText().toString().trim();
        String email = mUsernameView.getText().toString().trim();
        String password = mPasswordView.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid displayname, if the user entered one.
        if (!TextUtils.isEmpty(displayName) && !LoginHelper.isDisplayNameValid(displayName)) {
            mDisplayNameView.setError(getString(R.string.error_invalid_display_name));
            focusView = mDisplayNameView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !LoginHelper.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            if (null == focusView) {
                focusView = mPasswordView;
            }
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            if (null == focusView) {
                focusView = mUsernameView;
            }
            cancel = true;
        } else if (!LoginHelper.isEmailValid(email)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            if (null == focusView) {
                focusView = mUsernameView;
            }
            cancel = true;
        }

        if (cancel) {
            if (null != focusView) {
                focusView.requestFocus();
            }
        } else {
            showProgress(true);
            FBServiceUtils.getSigUpEvent(displayName, email, password).post();
        }

    }

    public void onEvent( IEvent aEvent ) {

        showProgress(false);
        final int lID = aEvent.getEventID();

        switch (lID) {
            case ILogin.SIGN_UP_SUCCESS: {
                User lUser = (User) aEvent.getEventObject();
                handleLoginSuccess(lUser);
                break;
            }
            case ILogin.SIGN_UP_FAILED: {
                Exception lException = (Exception) aEvent.getEventObject();
                Toast.makeText(this, lException.getMessage(), Toast.LENGTH_SHORT).show();
                break;
            }
            case ILogin.REQUEST_EMAIL_VERIFICATION: {
                Toast.makeText(this, R.string.war_email_verification, Toast.LENGTH_LONG).show();
                LaunchUtils.showLoginActivity(this);
            }
        }
    }

    private void handleLoginSuccess( User aUser ) {
        if (CommonUtils.DEBUG_LOG) {
            Log.d(TAG, "[LoginSuccess-Reason][" + aUser.getUserName() + "]");
        }
        finish();
        startActivity(new Intent(this, HomeActivity.class));
    }

    @Override
    public void onClick( View v ) {

        int lID = v.getId();

        if (lID == R.id.btn_signup) {
            attemptSignup();
        } else if (lID == R.id.btn_signin) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return false;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    protected void configureActionBar() {
    }

}
