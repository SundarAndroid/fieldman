
package com.alberichsoft.fieldman.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public abstract class BaseAdapter<D, T extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<T> {

    protected Context mContext = null;

    protected List<D> mEmptyList = new ArrayList<>(0);

    protected List<D> mFilteredList = mEmptyList;

    protected List<D> mDataList = mEmptyList;

    protected List<D> mDisplayedDataList = mDataList;

    protected Map<Integer, D> mSelectedMap;

    public abstract void loadData();

    public BaseAdapter(Context aContext) {
        mContext = aContext;
        mSelectedMap = new HashMap<Integer, D>();
        loadData();
    }

    @Override
    public int getItemCount() {
        return mDisplayedDataList.size();
    }

    public boolean isEmpty() {
        return (null == mDisplayedDataList || mDisplayedDataList.isEmpty());
    }

    public void setData( List<D> aDataList ) {

        if (null == aDataList) {
            mDisplayedDataList = mDataList = mEmptyList;
        } else {
            mDisplayedDataList = mDataList = aDataList;
        }
        notifyDataSetChanged();
    }

    public List<D>  getData() {
        return mDisplayedDataList;
    }

    public void setFilteredData( List<D> aDataList ) {
        if (null == aDataList) {
            mDisplayedDataList = mDataList = mEmptyList;
        } else {
            mDisplayedDataList = aDataList;
        }
        notifyDataSetChanged();
    }

    public void invalidateFilteredData() {
        notifyDataSetChanged();
    }

}
