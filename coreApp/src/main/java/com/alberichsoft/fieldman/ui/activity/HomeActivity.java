
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.utility.CommonUtils;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.helper.HomeHelper;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;

import static com.alberichsoft.common.model.UserRole.ADMIN;
import static com.alberichsoft.common.model.UserRole.MANAGER;
import static com.alberichsoft.common.model.UserRole.TECHNICIAN;
import static com.alberichsoft.common.msg.EventID.ILogin;
import static com.alberichsoft.common.msg.EventID.IUser;
import static com.alberichsoft.fieldman.utility.UiUtils.FullScreenDialog;
import static com.alberichsoft.fieldman.utility.UiUtils.getDialog;

public class HomeActivity extends BaseActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private TextView mUserNameTxtView = null;

    private TextView mUserTypeTxtView = null;

    private ImageView mUserTypeImageView = null;

    private FullScreenDialog mFDialog = null;

    private User mCurrentUser = null;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        FBServiceUtils.postGetUserEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null == mCurrentUser) {
            mFDialog.show();
        }
    }

    @Override
    protected void initViews() {
        setActionBarTitle(getString(R.string.app_name));
        mUserTypeImageView = (ImageView) findViewById(R.id.user_type_img_view);
        mUserNameTxtView = (TextView) findViewById(R.id.user_name_txt_view);
        mUserTypeTxtView = (TextView) findViewById(R.id.user_type_txt_view);

        View lLogout = findViewById(R.id.log_out);
        lLogout.setOnClickListener(this);

        mFDialog = getDialog(this, R.string.war_initializing, R.drawable.splash_logo, true);
    }

    @Override
    public void onClick( View aView ) {

        if (null != aView.getTag()) {

            int lTargetTagID = Integer.parseInt(aView.getTag().toString());

            if (R.string.dash_action_appointment == lTargetTagID) {
                startActivity(new Intent(this, AppointmentListActivity.class));
            } else if (R.string.dash_action_my_appointment == lTargetTagID) {
                startActivity(new Intent(this, MyAppointmentListActivity.class));
            } else if (R.string.dash_action_locate_appointment == lTargetTagID) {
                LaunchUtils.startMapActivityToShowTask(this);
            } else if (R.string.dash_action_my_profile == lTargetTagID) {
                startActivity(new Intent(this, MyProfileActivity.class));
            } else if (R.string.dash_action_settings == lTargetTagID) {
                startActivity(new Intent(this, SettingsActivity.class));
            } else if (R.string.dash_action_report == lTargetTagID) {
                startActivity(new Intent(this, ReportActivity.class));
            } else if (R.string.dash_action_manage_appointments == lTargetTagID) {
                startActivity(new Intent(this, ManageAppointmentActivity.class));
            } else {
                Toast.makeText(this, R.string.str_not_implemented, Toast.LENGTH_SHORT).show();
            }
        } else {
            int lViewID = aView.getId();
            if (R.id.log_out == lViewID) {
                FBServiceUtils.getSigOutEvent().post();
            }
        }
    }

    private void decideUser( User aUser ) {

        int aRole = -1;
        if (null == aUser.getRole() || !TextUtils.isDigitsOnly(aUser.getRole())) {
            aRole = TECHNICIAN.ordinal();
        } else {
            aRole = Integer.parseInt(aUser.getRole());
        }

        CommonUtils.printDLog(TAG, "Initializing UI for the user", "" + aRole);

        if (aRole == ADMIN.ordinal()) {
            initUIForAdminUser();
        } else if (aRole == MANAGER.ordinal()) {
            initUIForManager();
        } else { // TECHNICIAN
            initUIForTech();
        }
        updateBottomBar(aUser);
        mProgressView.dismiss();
    }

    private void initUIForAdminUser() {

        // Row:1
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_1),
                R.string.dash_action_appointment,
                R.drawable.ic_reports,
                R.string.dash_action_locate_appointment,
                R.drawable.ic_locate_appointment);

        // Row:2
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_2),
                R.string.dash_action_my_profile,
                R.drawable.ic_my_profile,
                R.string.dash_action_settings,
                R.drawable.ic_settings);

        // Row:3
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_3),
                R.string.dash_action_report,
                R.drawable.ic_usage_statistic,
                -1,
                -1);

        // Row:4
        HomeHelper.setGrid(this, findViewById(R.id.row_4), -1, -1, -1, -1);
    }

    private void initUIForManager() {
        // Row:1
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_1),
                R.string.dash_action_manage_appointments,
                R.drawable.ic_create_task,
                R.string.dash_action_locate_appointment,
                R.drawable.ic_locate_appointment);

        // Row:2
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_2),
                R.string.dash_action_my_profile,
                R.drawable.ic_my_profile,
                R.string.dash_action_settings,
                R.drawable.ic_settings);

        // Row:3
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_3),
                R.string.dash_action_report,
                R.drawable.ic_usage_statistic,
                -1,
                -1);

        // Row:4
        HomeHelper.setGrid(this, findViewById(R.id.row_4), -1, -1, -1, -1);
    }

    private void initUIForTech() {
        // Row:1
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_1),
                R.string.dash_action_my_appointment,
                R.drawable.ic_reports,
                R.string.dash_action_locate_appointment,
                R.drawable.ic_locate_appointment);

        // Row:2
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_2),
                R.string.dash_action_my_profile,
                R.drawable.ic_my_profile,
                R.string.dash_action_settings,
                R.drawable.ic_settings);

        // Row:3
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_3),
                R.string.dash_action_report,
                R.drawable.ic_usage_statistic,
                -1,
                -1);

        // Row:4
        HomeHelper.setGrid(this, findViewById(R.id.row_4), -1, -1, -1, -1);
    }

    private void updateBottomBar( User aUser ) {

        mUserNameTxtView.setText(aUser.getUserName());

        String lTempRole = aUser.getRole();
        if (!TextUtils.isEmpty(lTempRole)) {
            int lRole = Integer.parseInt(aUser.getRole());

            int lStringID = R.string.str_technician;
            int lDrawableID = R.drawable.ic_technician_small;
            if (lRole == 0) {
                lStringID = R.string.str_admin;
                lDrawableID = R.drawable.ic_admin_small;
            } else if (lRole == 1) {
                lStringID = R.string.str_manager;
                lDrawableID = R.drawable.ic_manager_small;
            } else if (lRole == 2) {
                lStringID = R.string.str_technician;
                lDrawableID = R.drawable.ic_technician_small;
            }
            mUserTypeTxtView.setText(lStringID);
            mUserTypeImageView.setImageResource(lDrawableID);
        } else {
            mUserTypeTxtView.setVisibility(View.GONE);
            mUserTypeImageView.setVisibility(View.GONE);
        }
    }

    public void onEvent( IEvent aEvent ) {
        int lEventID = aEvent.getEventID();
        switch (lEventID) {
            case IUser.GET_CURRENT_USER_SUCCESS: {
                mCurrentUser = (User) aEvent.getEventObject();
                if (null != mCurrentUser) {
                    decideUser(mCurrentUser);
                    mFDialog.dismiss();
                }
                break;
            }
            case ILogin.SHOW_LOGIN:
            case ILogin.AUTHENTICATION_FAILED:
            case IUser.GET_CURRENT_USER_FAILED: {
                mCurrentUser = null;
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
            }
            case IUser.CURRENT_USER_DATA_CHANGED: {
                FBServiceUtils.postGetUserEvent();
                break;
            }
        }
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return false;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }
}
