
package com.alberichsoft.fieldman.ui.custom;

import com.alberichsoft.common.utility.TypefaceCache;
import com.alberichsoft.fieldman.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * TextView with the ability to set fonts dynamically through XML layouts.
 */

public class CustomEditText extends EditText {

    public CustomEditText(Context context) {
        super(context, null);
        init(context, null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private Typeface myTypeface = null;

    private void init( final Context context, AttributeSet attrs ) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextview);
            String fontName = a.getString(R.styleable.CustomTextview_font);
            if (TextUtils.isEmpty(fontName)) {
                fontName = context.getResources().getString(R.string.font_gotham_light_regular);
            }
            myTypeface = TypefaceCache.getTypeface(context, fontName);

            setTypeface(myTypeface);
            a.recycle();
        }
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

            }

            @Override
            public void onTextChanged( CharSequence s, int start, int before, int count ) {
                CustomEditText et = CustomEditText.this;
                et.setTypeface(myTypeface);
            }

            @Override
            public void afterTextChanged( Editable s ) {

            }
        });
    }

}
