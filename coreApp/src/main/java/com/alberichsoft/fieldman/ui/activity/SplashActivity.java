
package com.alberichsoft.fieldman.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.alberichsoft.common.msg.EventID.ILogin;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.msg.IServiceEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.core.AppConstants;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;

/**
 * Created by santhanasamy_a on 7/7/2017.
 */

public class SplashActivity extends BaseActivity {

    private volatile boolean mIsLaunched = false;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void initViews() {
        View lView = findViewById(R.id.app_logo_view);
        lView.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (!mIsLaunched) {
                    mIsLaunched = true;
                    actBasedOnUserState();
                }
            }
        }, AppConstants.SPLASH_TIME_OUT);
    }

    public void onEvent( IEvent aEvent ) {

        final int lID = aEvent.getEventID();

        switch (lID) {

            case ILogin.IS_AUTHENTICATED: {

                boolean lIsAuthenticated = (boolean) aEvent.getEventObject();
                if (lIsAuthenticated) {
                    LaunchUtils.startHomeActivity(this);
                } else {
                    LaunchUtils.showLoginActivity(this);
                }
                break;
            }
        }
    }

    private void actBasedOnUserState() {

        new IServiceEvent.Builder()
                .setEventID(ILogin.IS_AUTHENTICATED)
                .setEventName("Check authentication state")
                .build()
                .post();
    }

    @Override
    public void onClick( View v ) {
        if (!mIsLaunched) {
            mIsLaunched = true;
            actBasedOnUserState();
        }
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return false;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    protected void configureActionBar() {
    }

}
