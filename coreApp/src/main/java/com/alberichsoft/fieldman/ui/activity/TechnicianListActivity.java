
package com.alberichsoft.fieldman.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.adapter.TechnicianAdapter;
import com.alberichsoft.fieldman.ui.base.PickerActivity;
import com.alberichsoft.fieldman.ui.custom.EmptyWarningView;
import com.alberichsoft.fieldman.ui.decoration.DividerDecoration;
import com.alberichsoft.fieldman.utility.UiUtils;

import java.util.List;

import static com.alberichsoft.common.msg.EventID.IUser;

/**
 * Created by santhanasamy_a on 7/17/2017.
 */

public class TechnicianListActivity extends PickerActivity {

    private RecyclerView mTaskRecyclerView = null;

    private TechnicianAdapter mTaskAdapter = null;

    private EmptyWarningView mEmptyWarningView = null;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technician_list);
    }

    @Override
    protected void initViews() {

        mEmptyWarningView = (EmptyWarningView) findViewById(R.id.empty_warning_view);
        mEmptyWarningView.setWarningText(getString(R.string.war_no_technician));
        mEmptyWarningView.setWarningImage(R.drawable.ic_change_task_state);
        mEmptyWarningView.setVisibility(View.VISIBLE);

        mTaskRecyclerView = (RecyclerView) findViewById(R.id.task_list_view);
        mTaskRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerDecoration itemDecoration = new DividerDecoration(
                this,
                R.dimen.task_list_item_divider_offset);
        mTaskRecyclerView.addItemDecoration(itemDecoration);

        mTaskAdapter = new TechnicianAdapter(this, getLaunchMode());
        mTaskRecyclerView.setAdapter(mTaskAdapter);
        mFProgressView = UiUtils.getDialog(
                this,
                R.string.war_loading_appointment,
                R.drawable.ic_assign_appointment,
                false);
        mFProgressView.show();

    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    public void configureActionbarForMode( PickerMode aPickerMode ) {
        setActionBarTitleAndLeftOnly(mTitleStringID);
    }

    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lEventID = aEvent.getEventID();

        if (IUser.GET_TECHNICIAN_LIST_SUCCESS == lEventID) {

            List<User> aUserList = (List<User>) aEvent.getEventObject();
            mTaskAdapter.setData(aUserList);

            updateEmptyViewState(null != aUserList && !aUserList.isEmpty());
            mFProgressView.dismiss();
        } else if (IUser.GET_TECHNICIAN_LIST_FAILED == lEventID) {
            mFProgressView.dismiss();
            updateEmptyViewState(false);
            Toast.makeText(this, "Failed to get user list", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateEmptyViewState( boolean hasData ) {
        mEmptyWarningView.setVisibility(hasData ? View.GONE : View.VISIBLE);
    }
}
