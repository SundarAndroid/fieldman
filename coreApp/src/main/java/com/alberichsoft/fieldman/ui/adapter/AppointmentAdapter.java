
package com.alberichsoft.fieldman.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.viewholder.AppointmentViewHolder;
import com.alberichsoft.fieldman.utility.LaunchUtils;

import static com.alberichsoft.fieldman.ui.activity.AppointmentDetailActivity.AppointmentMode;

/**
 * Created by santhanasamy_a on 7/10/2017.
 */

public class AppointmentAdapter extends BaseAdapter<Appointment, AppointmentViewHolder>
        implements View.OnClickListener {

    public AppointmentAdapter(Context aContext) {
        super(aContext);
    }

    @Override
    public void loadData() {

        // Note: As this operation is already done through the listener
        // new IServiceEvent.Builder()
        // .setEventID(EventID.IAppointment.GET_ALL_APPOINTMENTS)
        // .setEventObject(AppointmentStatus.OPEN)
        // .setEventName("Get All Appointments from FB Remote")
        // .build()
        // .post();
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder( ViewGroup parent, int viewType ) {
        View lView = LayoutInflater.from(mContext).inflate(R.layout.my_appointment_list_item, null);
        return new AppointmentViewHolder(lView);
    }

    @Override
    public void onBindViewHolder( AppointmentViewHolder aViewHolder, int aPosition ) {
        Appointment lAppointment = mDisplayedDataList.get(aPosition);
        if (null == aViewHolder || null == lAppointment) {
            return;
        }

        aViewHolder.getRootView().setOnClickListener(this);
        aViewHolder.getRootView().setTag(aPosition);
        aViewHolder.bindAppointmentInfo(mContext, lAppointment, aPosition);

    }

    @Override
    public void onClick( View aView ) {

        int lPosition = (int) aView.getTag();
        LaunchUtils.startAppointmentDetailActivity(
                (Activity) mContext,
                mDisplayedDataList.get(lPosition),
                AppointmentMode.ADMIN_ACTION,
                100);
    }

}
