
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.helper.HomeHelper;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;

/**
 * Created by santhanasamy_a on 7/9/2017.
 */

public class ManageAppointmentActivity extends BaseActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_appointment);
        setActionBarTitleAndLeftOnly(R.string.dash_action_manage_appointments);
    }

    @Override
    protected void initViews() {

        // Row:1
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_1),
                R.string.str_create_appointment,
                R.drawable.ic_create_appointment,
                R.string.str_assign_appointment,
                R.drawable.ic_assign_appointment);

        // Row:2
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_2),
                R.string.str_delete_appointment,
                R.drawable.ic_delete_appointment,
                R.string.str_manage_user,
                R.drawable.ic_manage_user);

        // Row:3
        HomeHelper.setGrid(
                this,
                findViewById(R.id.row_3),
                R.string.str_manage_technician,
                R.drawable.ic_manage_technician,
                -1,
                -1);

        // Row:4
        HomeHelper.setGrid(this, findViewById(R.id.row_4), -1, -1, -1, -1);
    }

    @Override
    public void onClick( View aView ) {

        super.onClick(aView);
        if (null != aView.getTag()) {

            int lTargetTagID = Integer.parseInt(aView.getTag().toString());
            if (R.string.str_create_appointment == lTargetTagID) {
                startActivity(new Intent(this, CreateAppointmentActivity.class));
            } else if (R.string.str_assign_appointment == lTargetTagID) {

                LaunchUtils.startAppointmentPickerActivity(
                        this,
                        R.string.str_select_appointment,
                        -1,
                        1);

            }
            // else if (R.string.str_delete_appointment == lTargetTagID) {
            // startActivity(new Intent(this, CreateAppointmentActivity.class));
            // } else if (R.string.str_manage_user == lTargetTagID) {
            // startActivity(new Intent(this, CreateAppointmentActivity.class));
            // } else if (R.string.str_manage_technician == lTargetTagID) {
            // startActivity(new Intent(this, CreateAppointmentActivity.class));
            // }
            else {
                Toast.makeText(this, R.string.str_not_implemented, Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

}
