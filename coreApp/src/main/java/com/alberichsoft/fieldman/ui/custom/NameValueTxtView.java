
package com.alberichsoft.fieldman.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alberichsoft.fieldman.R;

public class NameValueTxtView extends RelativeLayout {

    private TextView mNameTxtView = null;

    private TextView mValueTxtView = null;

    public NameValueTxtView(Context context) {
        this(context, null);
    }

    public NameValueTxtView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NameValueTxtView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init( final Context context, final AttributeSet attrs ) {

        // Set the values if provided in the layout XML
        if (attrs == null) {
            return;
        }
        // Read styleable for empty view
        final TypedArray lStyleArray = context
                .obtainStyledAttributes(attrs, R.styleable.NameValueTxtView);

        int lName = lStyleArray.getResourceId(R.styleable.NameValueTxtView_name, -1);
        int lValue = lStyleArray.getResourceId(R.styleable.NameValueTxtView_value, -1);

        int lNameTxtSize = (int) lStyleArray
                .getDimension(R.styleable.NameValueTxtView_name_text_size, -1);
        int lValueTxtSize = (int) lStyleArray
                .getDimension(R.styleable.NameValueTxtView_value_text_size, -1);

        int lResLayout = lStyleArray.getResourceId(R.styleable.NameValueTxtView_custom_layout, -1);

        final int lValueType = lStyleArray.getInt(R.styleable.NameValueTxtView_value_type, -1);

        View lView = null;
        if (-1 != lResLayout) {
            lView = LayoutInflater.from(context).inflate(lResLayout, this, true);
        } else {
            lView = LayoutInflater
                    .from(context)
                    .inflate(R.layout.common_name_value_item, this, true);
        }

        mNameTxtView = (TextView) lView.findViewById(R.id.txt_name);
        mValueTxtView = (TextView) lView.findViewById(R.id.txt_value);

        if (lValueType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            mValueTxtView.setInputType(
                    InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        if (-1 != lNameTxtSize) {
            mNameTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, lNameTxtSize);
        }
        if (-1 != lValueTxtSize) {
            mValueTxtView.setTextSize(TypedValue.COMPLEX_UNIT_PX, lValueTxtSize);
        }
        mNameTxtView.setText(getResources().getString(lName));
        mValueTxtView.setText(getResources().getString(lValue));
        // Recycle the styleable
        lStyleArray.recycle();
    }

    public void setName( String name ) {

        if (TextUtils.isEmpty(name)) {
            name = "";
        }
        mNameTxtView.setText(name);
    }

    public void setValue( String value ) {
        if (TextUtils.isEmpty(value)) {
            value = "";
        }
        mValueTxtView.setText(value);
    }

    public String getName() {
        return mNameTxtView.getText().toString();
    }

    public String getValue() {
        return mValueTxtView.getText().toString();
    }

}
