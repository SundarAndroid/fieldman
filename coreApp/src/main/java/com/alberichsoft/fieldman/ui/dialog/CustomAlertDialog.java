
package com.alberichsoft.fieldman.ui.dialog;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.utility.GlideImageLoader;

/**
 * Created by santhanasamy_a on 6/7/2017.
 */

public final class CustomAlertDialog extends Dialog {

    private Context mContext = null;

    private TextView mDialogHeaderView = null;

    private TextView mLeftTextView = null;

    private TextView mRightTextView = null;

    private ImageView mLeftImageView = null;

    private ImageView mRightImageView = null;

    private TextView mLeftActionTextView = null;

    private TextView mRightActionTextView = null;

    private void init(
            Context aContext,
            Builder aBuilder,
            String aTechnicianImageUrl,
            String aCustomerImageUrl ) {

        mContext = aContext;
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater layoutInflater = (LayoutInflater) aContext
                .getSystemService(Service.LAYOUT_INFLATER_SERVICE);
        View lView = layoutInflater.inflate(R.layout.assign_technician_dialog_layout, null);
        setContentView(lView);

        mDialogHeaderView = (TextView) lView.findViewById(R.id.dialog_header_txt_view);
        mLeftTextView = (TextView) lView.findViewById(R.id.left_txt_view);
        mRightTextView = (TextView) lView.findViewById(R.id.right_txt_view);

        ImageView lAssignArrowView = (ImageView) lView.findViewById(R.id.mid_arrow_view);

        mLeftImageView = (ImageView) lView.findViewById(R.id.left_img);
        if (!TextUtils.isEmpty(aTechnicianImageUrl)) {

            GlideImageLoader.loadImage(
                    mLeftImageView,
                    aTechnicianImageUrl,
                    R.drawable.ic_technician,
                    R.drawable.ic_technician,
                    true);
        }

        mRightImageView = (ImageView) lView.findViewById(R.id.right_img);
        if (!TextUtils.isEmpty(aCustomerImageUrl)) {

            GlideImageLoader.loadImage(
                    mRightImageView,
                    aCustomerImageUrl,
                    R.drawable.ic_user,
                    R.drawable.ic_user,
                    true);
        }

        mLeftImageView.setImageResource(aBuilder.mLeftImgID);
        mRightImageView.setImageResource(aBuilder.mRightImgID);
        lAssignArrowView.setImageResource(aBuilder.mMidArrowImgID);

        mDialogHeaderView.setText(aBuilder.mTitleText);
        mLeftTextView.setText(aBuilder.mLeftText);
        mRightTextView.setText(aBuilder.mRightText);

        mLeftActionTextView = (TextView) lView.findViewById(R.id.right_action_btn);
        mRightActionTextView = (TextView) lView.findViewById(R.id.left_action_btn);

        mLeftActionTextView.setOnClickListener(aBuilder.mOnClickListener);
        mRightActionTextView.setOnClickListener(aBuilder.mOnClickListener);

        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.dimAmount = 0.7f;
        // wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setWindowAnimations(R.style.default_dialog_animation);
        window.setAttributes(wlp);
        // (int)
        // aContext.getResources().getDimension(R.dimen.app_warning_alert_height)
        window.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
    }

    private CustomAlertDialog(Context aContext, Builder aBuilder) {
        super(aContext, android.R.style.Theme_Translucent_NoTitleBar);
        init(aContext, aBuilder, null, null);
    }

    private CustomAlertDialog(
            Context aContext,
            Builder aBuilder,
            String aTechnicianImageUrl,
            String aCustomerImageUrl) {
        super(aContext, android.R.style.Theme_Translucent_NoTitleBar);
        init(aContext, aBuilder, aTechnicianImageUrl, aCustomerImageUrl);
    }

    public void set( String aLeftTxtView, String aRightTxtView ) {
        mLeftTextView.setText(aLeftTxtView);
        mRightTextView.setText(aRightTxtView);
    }

    public void set(
            String aLeftTxtView,
            String aRightTxtView,
            String aTechnicianURI,
            String aCustomerURI ) {
        mLeftTextView.setText(aLeftTxtView);
        mRightTextView.setText(aRightTxtView);
        if (!TextUtils.isEmpty(aTechnicianURI)) {

            GlideImageLoader.loadImage(
                    mLeftImageView,
                    aTechnicianURI,
                    R.drawable.ic_technician,
                    R.drawable.ic_technician,
                    true);
        }

        if (!TextUtils.isEmpty(aCustomerURI)) {

            GlideImageLoader.loadImage(
                    mRightImageView,
                    aCustomerURI,
                    R.drawable.ic_user,
                    R.drawable.ic_user,
                    true);
        }
    }

    public static CustomAlertDialog getDialog(
            Context aContext,
            View.OnClickListener aListener,
            int aDeviceNameStr,
            int aUserNameStr ) {

        return new Builder(aContext)
                .setLeftImgID(R.drawable.ic_manage_technician)
                .setRightImgID(R.drawable.ic_user)
                .setMidArrowImgID(R.drawable.ic_right_arrow)
                .setLeftText(aContext.getResources().getString(aDeviceNameStr))
                .setRightText(aContext.getResources().getString(aUserNameStr))
                .setOnClickListener(aListener)
                .build();
    }

    public static final class Builder {

        private View.OnClickListener mOnClickListener;

        private Context mContext;

        private String mTitleText;

        private String mLeftText;

        private String mRightText;

        private String mRightActionText;

        private String mLeftActionText;

        private int mLeftImgID;

        private int mRightImgID;

        public int mMidArrowImgID;

        public Builder(Context aContext) {
            mContext = aContext;
        }

        public Builder setOnClickListener( View.OnClickListener aOnClickListener ) {
            mOnClickListener = aOnClickListener;
            return this;
        }

        public Builder setTitleText( String aTitleText ) {
            mTitleText = aTitleText;
            return this;
        }

        public Builder setLeftText( String aLeftText ) {
            mLeftText = aLeftText;
            return this;
        }

        public Builder setRightText( String aRightText ) {
            mRightText = aRightText;
            return this;
        }

        public Builder setLeftActionText( String aLeftText ) {
            mLeftActionText = aLeftText;
            return this;
        }

        public Builder setRightActionText( String aRightText ) {
            mRightActionText = aRightText;
            return this;
        }

        public Builder setLeftImgID( int aLeftImgID ) {
            mLeftImgID = aLeftImgID;
            return this;
        }

        public Builder setMidArrowImgID( int aMidArrowImgID ) {
            mMidArrowImgID = aMidArrowImgID;
            return this;
        }

        public Builder setRightImgID( int aRightImgID ) {
            mRightImgID = aRightImgID;
            return this;
        }

        public Builder setListner( View.OnClickListener aOnClickListener ) {
            mOnClickListener = aOnClickListener;
            return this;
        }

        public CustomAlertDialog build() {
            return new CustomAlertDialog(mContext, this);
        }

        public CustomAlertDialog build( String aTechnicianImageUrl, String aCustomerImageUrl ) {
            return new CustomAlertDialog(mContext, this, aTechnicianImageUrl, aCustomerImageUrl);
        }
    }
}
