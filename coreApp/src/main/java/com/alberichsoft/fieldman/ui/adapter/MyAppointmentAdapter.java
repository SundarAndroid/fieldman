
package com.alberichsoft.fieldman.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.alberichsoft.common.msg.EventID;
import com.alberichsoft.common.msg.IServiceEvent;
import com.alberichsoft.fieldman.utility.LaunchUtils;

import static com.alberichsoft.fieldman.ui.activity.AppointmentDetailActivity.AppointmentMode;

/**
 * Created by santhanasamy_a on 7/10/2017.
 */

public class MyAppointmentAdapter extends AppointmentAdapter {

    public MyAppointmentAdapter(Context aContext) {
        super(aContext);
    }

    @Override
    public void loadData() {
        // Note: As this operation is already done through the listener
        new IServiceEvent.Builder()
                .setEventID(EventID.IAppointment.GET_USER_APPOINTMENTS)
                .setEventName("Get User Appointments from FB Remote")
                .build()
                .post();
    }

    @Override
    public void onClick( View aView ) {

        int lPosition = (int) aView.getTag();
        LaunchUtils.startAppointmentDetailActivity(
                (Activity) mContext,
                mDisplayedDataList.get(lPosition),
                AppointmentMode.USER_ACTION,
                100);
    }

}
