
package com.alberichsoft.fieldman.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.AppointmentStatus;
import com.alberichsoft.common.model.Customer;
import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.ui.base.PickerActivity;
import com.alberichsoft.fieldman.ui.dialog.CustomAlertDialog;
import com.alberichsoft.fieldman.utility.LaunchUtils;
import com.alberichsoft.fieldman.utility.UiUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;

import static com.alberichsoft.common.msg.EventID.IAppointment;
import static com.alberichsoft.fieldman.utility.UiUtils.getDisplayableDate;
import static com.alberichsoft.fieldman.utility.UiUtils.getTaskStatusString;

/**
 * Created by santhanasamy_a on 7/10/2017.
 */

public class AppointmentDetailActivity extends BaseActivity {

    private static final int REQUEST_PICK_TECHNICIAN = 1000;

    public enum AppointmentMode {

        ADMIN_ACTION, USER_ACTION;
        public static final String EXTRA_KEY_LAUNCH_MODE = "AppointmentDetail_Launch_Mode";
    }

    private TextView userName = null;

    private TextView mobileNoView = null;

    private TextView emailView = null;

    private TextView addressView = null;

    private ImageView profileImageView = null;

    private TextView mCreatedDateView = null;

    private TextView mStartDateView = null;

    private TextView mEndDateView = null;

    private TextView taskStatusView = null;

    private TextView taskreviewView = null;

    private Button mAssignTaskButton = null;

    private Button mStartStopTaskButton = null;

    private CustomAlertDialog mReturnDialog = null;

    private Appointment mAppointment = null;

    private AppointmentMode mode = AppointmentMode.USER_ACTION;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_detail);
    }

    @Override
    protected void initViews() {

        Bundle lBundle = getIntent().getExtras();
        mAppointment = lBundle.getParcelable(PickerActivity.EXTRA_KEY_OBJECT);

        int lTitleID = lBundle.getInt(PickerActivity.EXTRA_KEY_TITLE, -1);
        if (lTitleID == -1) {
            setActionBarTitleAndLeftOnly(R.string.str_task_detail);
        } else {
            setActionBarTitleAndLeftOnly(lTitleID);
        }

        int lModeOrdinal = lBundle.getInt(
                AppointmentMode.EXTRA_KEY_LAUNCH_MODE,
                AppointmentMode.USER_ACTION.ordinal());
        if (lModeOrdinal == AppointmentMode.USER_ACTION.ordinal()) {
            mode = AppointmentMode.USER_ACTION;
        } else if (lModeOrdinal == AppointmentMode.ADMIN_ACTION.ordinal()) {
            mode = AppointmentMode.ADMIN_ACTION;
        }

        Customer lCustomer = mAppointment.getCustomer();
        // lBundle.getParcelable(PickerActivity.EXTRA_KEY_OBJECT_1);

        mFProgressView = UiUtils.getDialog(this, R.string.war_loading, R.drawable.ic_work);

        userName = (TextView) findViewById(R.id.username_txt_text);

        mobileNoView = (TextView) findViewById(R.id.mobile_text_view);

        emailView = (TextView) findViewById(R.id.email_text_view);

        addressView = (TextView) findViewById(R.id.address_text_view);

        profileImageView = (ImageView) findViewById(R.id.profile_image_view);

        taskStatusView = (TextView) findViewById(R.id.task_status_txt_view);

        mCreatedDateView = (TextView) findViewById(R.id.task_complaint_at_txt_view);

        mStartDateView = (TextView) findViewById(R.id.task_started_at_txt_view);

        mEndDateView = (TextView) findViewById(R.id.task_end_at_txt_view);

        mAssignTaskButton = (Button) findViewById(R.id.assign_task_btn);

        mStartStopTaskButton = (Button) findViewById(R.id.start_stop_task_btn);

        taskreviewView = (TextView) findViewById(R.id.task_review_txt_view);

        mAssignTaskButton.setOnClickListener(this);
        mStartStopTaskButton.setOnClickListener(this);

        FBServiceUtils.registerSingleValueListener(mAppointment.getAppointmentID());
        invalidateViewDate();
    }

    private void invalidateViewDate() {

        Customer lCustomer = mAppointment.getCustomer();

        if (null != lCustomer) {
            userName.setText(lCustomer.getUserName());
            mobileNoView.setText(lCustomer.getMobileNumber());
            emailView.setText(lCustomer.getEmail());
            addressView.setText(lCustomer.getAddress());
        }

        taskStatusView.setText(getTaskStatusString(mAppointment.getStatus()));
        mCreatedDateView.setText(getDisplayableDate(Long.parseLong(mAppointment.getCreatedTime())));

        if (TextUtils.isEmpty(mAppointment.getStartedTime())) {
            mStartDateView.setText("-");
            mStartStopTaskButton.setText(R.string.str_start_appointment);
        } else {
            mStartStopTaskButton.setText(R.string.str_end_appointment);
            mStartDateView
                    .setText(getDisplayableDate(Long.parseLong(mAppointment.getStartedTime())));
        }

        if (TextUtils.isEmpty(mAppointment.getEndTime())) {
            mEndDateView.setText("-");
        } else {
            mEndDateView.setText(getDisplayableDate(Long.parseLong(mAppointment.getEndTime())));
            mStartStopTaskButton.setVisibility(View.GONE);
        }

        if (mode == AppointmentMode.ADMIN_ACTION) {
            mAssignTaskButton.setVisibility(
                    (mAppointment.getStatus() == AppointmentStatus.OPEN.ordinal()) ? View.VISIBLE
                            : View.GONE);
            mStartStopTaskButton.setVisibility(View.GONE);
        } else {
            mAssignTaskButton.setVisibility(View.GONE);
            mStartStopTaskButton.setVisibility(
                    TextUtils.isEmpty(mAppointment.getEndTime()) ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FBServiceUtils.unRegisterSingleValueListener(mAppointment.getAppointmentID());
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    public void onClick( View aView ) {
        super.onClick(aView);

        int lViewID = aView.getId();
        if (lViewID == R.id.assign_task_btn) {
            LaunchUtils.startTechnicianPicker(
                    this,
                    R.string.str_select_technician,
                    REQUEST_PICK_TECHNICIAN);
        } else if (lViewID == R.id.start_stop_task_btn) {
            mFProgressView.setWarImage(R.drawable.ic_change_task_state);

            if (getString(R.string.str_start_appointment).equals(((TextView) aView).getText())) {
                mFProgressView.setWarMessageTxt(R.string.war_appointment_start);
                FBServiceUtils.postStartTaskEvent(mAppointment);
            } else {
                mFProgressView.setWarMessageTxt(R.string.war_appointment_finish);
                FBServiceUtils.postFinishTaskEvent(mAppointment);
            }
            mFProgressView.show();

        } else if (lViewID == R.id.right_action_btn) {

            if (null != mReturnDialog && mReturnDialog.isShowing()) {
                FBServiceUtils.postAssignTechnicianEvent(mAppointment);
                mReturnDialog.dismiss();
                mFProgressView.showProgress(R.string.war_assigning_appointment);
            }

        } else if (lViewID == R.id.left_action_btn) {
            if (null != mReturnDialog) {
                mReturnDialog.dismiss();
            }
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {

        if (REQUEST_PICK_TECHNICIAN == requestCode) {

            if (null != data) {
                User lUser = data.getParcelableExtra(PickerActivity.BUNDLE_KEY_SINGLE_PICK_RESULT);
                mAppointment.setTechnicianID(lUser.getUserID());
                showConfirmationDialog(lUser, mAppointment);
            }
        }
    }

    private void showConfirmationDialog( User aTechnician, Appointment aAppointment ) {

        if (null == mReturnDialog) {
            mReturnDialog = new CustomAlertDialog.Builder(this)
                    .setLeftImgID(R.drawable.ic_technician)
                    .setRightImgID(R.drawable.ic_user)
                    .setMidArrowImgID(R.drawable.ic_assign_appointment_1)
                    .setTitleText(getString(R.string.str_assign_appointment))
                    .setLeftText(aTechnician.getUserName())
                    .setRightText(aAppointment.getCustomer().getUserName())
                    .setOnClickListener(this)
                    .build(aTechnician.getImageUrl(), aAppointment.getCustomer().getPhotoUrl());
        } else {
            mReturnDialog.set(aTechnician.getUserName(), aAppointment.getCustomer().getUserName(), aTechnician.getImageUrl(), aAppointment.getCustomer().getPhotoUrl());
        }
        mReturnDialog.show();
    }

    @Override
    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lEventID = aEvent.getEventID();

        if (IAppointment.ASSIGN_APPOINTMENT_SUCCESS == lEventID) {
            mFProgressView.dismiss();
            Toast
                    .makeText(this, R.string.war_assign_appointment_success, Toast.LENGTH_SHORT)
                    .show();
            finish();
        } else if (IAppointment.ASSIGN_APPOINTMENT_FAILED == lEventID) {
            mFProgressView.dismiss();
            Toast.makeText(this, R.string.war_assign_appointment_failed, Toast.LENGTH_SHORT).show();
        } else if (IAppointment.START_APPOINTMENT_SUCCESS == lEventID) {
            Toast.makeText(this, R.string.war_appointment_start_success, Toast.LENGTH_SHORT).show();
            mStartStopTaskButton.setText(R.string.str_end_appointment);
        } else if (IAppointment.START_APPOINTMENT_FAILED == lEventID) {
            mFProgressView.dismiss();
            Toast.makeText(this, R.string.war_appointment_start_failed, Toast.LENGTH_SHORT).show();
        } else if (IAppointment.FINISH_APPOINTMENT_SUCCESS == lEventID) {
            Toast
                    .makeText(this, R.string.war_appointment_finish_success, Toast.LENGTH_SHORT)
                    .show();
            mStartStopTaskButton.setVisibility(View.GONE);
        } else if (IAppointment.FINISH_APPOINTMENT_FAILED == lEventID) {
            mFProgressView.dismiss();
            Toast.makeText(this, R.string.war_appointment_finish_failed, Toast.LENGTH_SHORT).show();
        } else if (IAppointment.CURRENT_APPOINTMENT_DATA_CHANGED == lEventID) {
            mFProgressView.dismiss();
            mAppointment = (Appointment) aEvent.getEventObject();
            invalidateViewDate();
        }

    }
}
