
package com.alberichsoft.fieldman.ui.custom;

import com.alberichsoft.fieldman.R;
import com.alberichsoft.common.utility.TypefaceCache;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * TextView with the ability to set fonts dynamically through XML layouts.
 */

public class CustomTextView extends TextView {

    public CustomTextView(Context context) {
        super(context, null);
        init(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init( Context context, AttributeSet attrs ) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextview);
            String fontName = a.getString(R.styleable.CustomTextview_font);
            if (!TextUtils.isEmpty(fontName)) {

                Typeface myTypeface = TypefaceCache.getTypeface(context, fontName);
                if (!Typeface.DEFAULT.equals(myTypeface)) {
                    setTypeface(myTypeface);
                }
            }
            a.recycle();
        }
    }
}
