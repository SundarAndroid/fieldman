
package com.alberichsoft.fieldman.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.model.UserRole;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.custom.CustomTextView;
import com.alberichsoft.fieldman.utility.GlideImageLoader;

import static com.alberichsoft.fieldman.ui.base.PickerActivity.PickerMode;

/**
 * Created by santhanasamy_a on 9/26/2016.
 */

public class TechnicianViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mUserImage;

    private final CustomTextView mUserName;

    private final CustomTextView mEmail;

    private final CustomTextView mUserType;

    private final CheckBox mCheckbox;

    private final View rootView;

    public TechnicianViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        mUserImage = (ImageView) itemView.findViewById(R.id.user_image_view);
        mUserName = (CustomTextView) itemView.findViewById(R.id.user_name_view);
        mEmail = (CustomTextView) itemView.findViewById(R.id.user_email_view);
        mUserType = (CustomTextView) itemView.findViewById(R.id.user_type_view);
        mCheckbox = (CheckBox) itemView.findViewById(R.id.user_selection_checkbox);
    }

    public void bindTechnicianInfo( User aUser, PickerMode aLaunchMode, boolean aIsSelected ) {

        if (!TextUtils.isEmpty(aUser.getUserName())) {
            mUserName.setText(aUser.getUserName());
            mUserName.setVisibility(View.VISIBLE);
        } else {
            mUserName.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(aUser.getEmail())) {
            mEmail.setText(aUser.getEmail());
            mEmail.setVisibility(View.VISIBLE);
        } else {
            mEmail.setVisibility(View.GONE);
        }

        String lRole = mEmail
                .getResources()
                .getString(UserRole.getRoleName(Integer.parseInt(aUser.getRole())));
        if (!TextUtils.isEmpty(lRole)) {
            mUserType.setText(lRole);
            mUserType.setVisibility(View.VISIBLE);
        } else {
            mUserType.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(aUser.getImageUrl())) {
            GlideImageLoader.loadImage(
                    mUserImage,
                    aUser.getImageUrl(),
                    R.drawable.ic_user,
                    R.drawable.ic_user,
                    true);
        }

        if (PickerMode.SINGLE_PICK == aLaunchMode) {
            mCheckbox.setVisibility(View.VISIBLE);
            mCheckbox.setChecked(aIsSelected);
        } else {
            mCheckbox.setVisibility(View.GONE);
        }
    }

    public View getRootView() {
        return rootView;
    }

}
