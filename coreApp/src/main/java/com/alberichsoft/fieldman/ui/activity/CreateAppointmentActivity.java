
package com.alberichsoft.fieldman.ui.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.AppointmentStatus;
import com.alberichsoft.common.model.Customer;
import com.alberichsoft.common.model.Location;
import com.alberichsoft.common.msg.EventID;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.helper.LoginHelper;
import com.alberichsoft.fieldman.ui.base.BaseActivity;
import com.alberichsoft.fieldman.utility.LaunchUtils;
import com.alberichsoft.fieldman.utility.UiUtils;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by santhanasamy_a on 7/11/2017.
 */

public class CreateAppointmentActivity extends BaseActivity {

    private static final int PLACE_PICKER_REQUEST = 1;

    private LinearLayout locationContainerView = null;

    private TextView mUserName = null;

    private TextView mMobileNoView = null;

    private TextView mEmailView = null;

    private TextView mAddressView = null;

    private ImageView profileImageView = null;

    private Button assignmentDateView = null;

    private TextView assignEndDateView = null;

    private TextView taskStatusView = null;

    private TextView mTaskCommentView = null;

    private ImageView locateCustomerView = null;

    private TextView mLatView = null;

    private TextView mLongView = null;

    private LatLng mLatLong = null;

    private long mPickedTime = 0;;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appointment);
        setActionBarTitle(R.string.str_create_task);
        setRightActionText(R.string.str_save);
    }

    @Override
    protected void initViews() {

        mUserName = (TextView) findViewById(R.id.username_txt_text);

        mMobileNoView = (TextView) findViewById(R.id.mobile_text_view);

        mEmailView = (TextView) findViewById(R.id.email_text_view);

        mAddressView = (TextView) findViewById(R.id.address_text_view);

        profileImageView = (ImageView) findViewById(R.id.profile_image_view);

        taskStatusView = (TextView) findViewById(R.id.task_status_txt_view);

        mTaskCommentView = (TextView) findViewById(R.id.task_comment_txt_view);

        assignmentDateView = (Button) findViewById(R.id.task_complaint_at_txt_view);
        assignmentDateView.setOnClickListener(this);

        locateCustomerView = (ImageView) findViewById(R.id.locate_customer_view);
        locateCustomerView.setOnClickListener(this);

        locationContainerView = (LinearLayout) findViewById(R.id.location_container_view);
        mLatView = (TextView) locationContainerView.findViewById(R.id.lat_view);
        mLongView = (TextView) locationContainerView.findViewById(R.id.long_view);
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    public void onClick( View aView ) {
        super.onClick(aView);

        int lID = aView.getId();

        if (lID == R.id.task_complaint_at_txt_view) {
            showDateTimePicker();
        } else if (lID == R.id.locate_customer_view) {
            LaunchUtils.startMapActivityToPickLocation(this, PLACE_PICKER_REQUEST);
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {

        if (PLACE_PICKER_REQUEST == requestCode) {
            if (RESULT_OK == resultCode) {
                mLatLong = data
                        .getParcelableExtra(LocateAppointmentActivity.EXTRA_KEY_PICKER_RESULT);
                locationContainerView.setVisibility(View.VISIBLE);
                mLatView.setText("" + mLatLong.latitude);
                mLongView.setText("" + mLatLong.longitude);
            }
        }
    }

    @Override
    public void onEvent( IEvent aEvent ) {
        super.onEvent(aEvent);

        int lID = aEvent.getEventID();
        if (EventID.IAppointment.CREATE_APPOINTMENT_SUCCESS == lID) {
            showProgress(false);
            finish();
        } else if (EventID.IAppointment.CREATE_APPOINTMENT_FAILED == lID) {
            showProgress(false);
            Toast.makeText(this, "Not able to save Appointment", Toast.LENGTH_SHORT).show();
        }
    }

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        final Calendar lDate = Calendar.getInstance();
        new DatePickerDialog(
                mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(
                            DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth ) {
                        lDate.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(
                                mContext,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(
                                            TimePicker view,
                                            int hourOfDay,
                                            int minute ) {
                                        lDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        lDate.set(Calendar.MINUTE, minute);

                                        onDatePicked(lDate.getTime());
                                    }
                                },
                                currentDate.get(Calendar.HOUR_OF_DAY),
                                currentDate.get(Calendar.MINUTE),
                                false)
                                        .show();
                    }
                },
                currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DATE)).show();
    }

    private void onDatePicked( Date aTime ) {
        mPickedTime = aTime.getTime();
        assignmentDateView.setText(UiUtils.getDisplayableDate(mPickedTime));
    }

    @Override
    protected void onRightActionClicked( View aRightActionTxtView ) {

        if (validateDate()) {

        } else {

        }
    }

    private boolean validateDate() {

        // Reset errors.
        mUserName.setError(null);
        mMobileNoView.setError(null);
        mEmailView.setError(null);

        // Store values at the time of the login attempt.
        String lDisplayName = mUserName.getText().toString().trim();
        String lMobileNo = mMobileNoView.getText().toString().trim();
        String lEmail = mEmailView.getText().toString().trim();

        if (0 == mPickedTime) {
            Toast.makeText(this, "Please choose assignment date", Toast.LENGTH_SHORT).show();
        }

        boolean cancel = false;
        View focusView = null;

        // Check for a valid displayname, if the user entered one.
        if (!TextUtils.isEmpty(lDisplayName) && !LoginHelper.isDisplayNameValid(lDisplayName)) {
            mUserName.setError(getString(R.string.error_invalid_display_name));
            focusView = mUserName;
            cancel = true;
        }

        // Check for a valid mobile-no, if the user entered one.
        if (null == focusView && !TextUtils.isEmpty(lMobileNo)
                && !LoginHelper.isMobileNoValid(lMobileNo)) {
            mMobileNoView.setError(getString(R.string.error_invalid_mobile_no));
            if (null == focusView) {
                focusView = mMobileNoView;
            }
            cancel = true;
        }

        // Check for a valid email address.
        if (null == focusView && TextUtils.isEmpty(lEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            if (null == focusView) {
                focusView = mEmailView;
            }
            cancel = true;
        } else if (null == focusView && !LoginHelper.isEmailValid(lEmail)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            if (null == focusView) {
                focusView = mEmailView;
            }
            cancel = true;
        } else if (null == focusView && TextUtils.isEmpty(mAddressView.getText().toString())) {
            mAddressView.setError(getString(R.string.error_field_required));
            if (null == focusView) {
                focusView = mAddressView;
            }
            cancel = true;
        } else if (null == mLatLong) {
            Toast.makeText(this, "Please choose customer location", Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        if (cancel) {
            if (null != focusView) {
                focusView.requestFocus();
            }
        } else {
            showProgress(true);
            Appointment lAppointment = createAssignment();
            lAppointment.setCustomer(createCustomer());
            FBServiceUtils.postCreateAppointmentEvent(lAppointment);
        }
        return false;
    }

    private Appointment createAssignment() {
        Appointment lAppointment = new Appointment();
        lAppointment.setStatus(AppointmentStatus.OPEN.ordinal());
        lAppointment.setLocation(new Location(mLatLong.latitude, mLatLong.longitude));
        lAppointment.setCreatedTime("" + new Date().getTime());

        String lTaskComment = mTaskCommentView.getText().toString();
        if (!TextUtils.isEmpty(lTaskComment)) {
            lAppointment.setComment(lTaskComment.trim());
        }

        return lAppointment;
    }

    private Customer createCustomer() {
        Customer lCustomer = new Customer();
        lCustomer.setUserName(mUserName.getText().toString());
        lCustomer.setMobileNumber(mMobileNoView.getText().toString());
        lCustomer.setEmail(mEmailView.getText().toString());
        lCustomer.setAddress(mAddressView.getText().toString());
        lCustomer.setPhotoUrl("");
        return lCustomer;
    }
}
