
package com.alberichsoft.fieldman.core;

/**
 * General application constants.
 */
public class AppConstants {

    public static final int SPLASH_TIME_OUT = 2000;

    private AppConstants() {

    }

}
