
package com.alberichsoft.fieldman.core;

import android.app.Application;

import com.alberichsoft.common.msg.IServiceEvent;
import com.alberichsoft.fmservice.fire.core.FBInitializer;
import com.alberichsoft.fmservice.managers.FBManager;

import de.greenrobot.event.EventBus;

/**
 * Created by santhanasamy_a on 7/7/2017.
 */

public class FieldManApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.getDefault().register(this);
        FBInitializer.getInstance().initialize(this);
    }

    public void onEvent( IServiceEvent aEvent ) {
        FBManager.getInstance().onEvent(aEvent);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        EventBus.getDefault().unregister(this);
    }
}
