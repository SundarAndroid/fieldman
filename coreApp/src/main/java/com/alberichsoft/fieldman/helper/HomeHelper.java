
package com.alberichsoft.fieldman.helper;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberichsoft.fieldman.R;

/**
 * Created by santhanasamy_a on 9/24/2016.
 */

public class HomeHelper {

    public static void setGrid(
            View.OnClickListener aListener,
            View lView,
            int titleTxt1,
            int titleImg1,
            int titleTxt2,
            int titleImg2 ) {
        if (-1 == titleTxt1 && -1 == titleImg1 && -1 == titleTxt2 && -1 == titleImg2) {
            lView.setVisibility(View.GONE);
            return;
        }
        View lContainer1 = lView.findViewById(R.id.col_1_container);
        lContainer1.setVisibility(View.VISIBLE);
        if (-1 != titleTxt1 && -1 != titleImg1) {
            lContainer1.setTag(titleTxt1);
            lContainer1.setOnClickListener(aListener);
            TextView lBtn1 = (TextView) lView.findViewById(R.id.btn_1);
            lBtn1.setText(titleTxt1);
            ImageView lImg1 = (ImageView) lView.findViewById(R.id.img_view_1);
            lImg1.setImageResource(titleImg1);
            lImg1.bringToFront();
        } else {
            lContainer1.setVisibility(View.INVISIBLE);
        }

        View lContainer2 = lView.findViewById(R.id.col_2_container);
        lContainer2.setVisibility(View.VISIBLE);
        if (-1 != titleTxt2 && -1 != titleImg2) {
            lContainer2.setTag(titleTxt2);
            lContainer2.setOnClickListener(aListener);
            TextView lBtn2 = (TextView) lView.findViewById(R.id.btn_2);
            lBtn2.setText(titleTxt2);
            ImageView lImg2 = (ImageView) lView.findViewById(R.id.img_view_2);
            lImg2.setImageResource(titleImg2);
            lImg2.bringToFront();
        } else {
            lContainer2.setVisibility(View.INVISIBLE);
        }

    }

    public static Dialog getDiscardWarningDialog(
            Context aContext,
            DialogInterface.OnClickListener aListener ) {
        return new AlertDialog.Builder(aContext)
                .setCancelable(false)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(R.string.app_name)
                .setPositiveButton(android.R.string.ok, aListener)
                .setNegativeButton(android.R.string.cancel, aListener)
                .create();
    }

}
