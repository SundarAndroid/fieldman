
package com.alberichsoft.fieldman.helper;

import android.telephony.PhoneNumberUtils;

/**
 * Created by santhanasamy_a on 9/22/2016.
 */

public class LoginHelper {

    private static final String TAG = LoginHelper.class.getSimpleName();

    public static boolean isEmailValid( String email ) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid( String password ) {
        return password.length() > 4;
    }

    public static boolean isDisplayNameValid( String displayName ) {
        return displayName.length() > 4;
    }

    public static boolean isMobileNoValid( String mobileNo ) {

        // android.util.Patterns.PHONE.matcher(mobileNo).matches();
        return PhoneNumberUtils.isGlobalPhoneNumber(mobileNo);
    }

}
