
package com.alberichsoft.fieldman.utility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.User;
import com.alberichsoft.fieldman.R;
import com.alberichsoft.fieldman.ui.activity.AppointmentDetailActivity;
import com.alberichsoft.fieldman.ui.activity.AppointmentListActivity;
import com.alberichsoft.fieldman.ui.activity.HomeActivity;
import com.alberichsoft.fieldman.ui.activity.LocateAppointmentActivity;
import com.alberichsoft.fieldman.ui.activity.LoginActivity;
import com.alberichsoft.fieldman.ui.activity.SignUpActivity;
import com.alberichsoft.fieldman.ui.activity.TechnicianListActivity;
import com.alberichsoft.fieldman.ui.base.PickerActivity;

import static android.app.Activity.RESULT_OK;
import static com.alberichsoft.fieldman.ui.activity.AppointmentDetailActivity.AppointmentMode;
import static com.alberichsoft.fieldman.ui.activity.LocateAppointmentActivity.EXTRA_KEY_LAUNCH_MODE;
import static com.alberichsoft.fieldman.ui.activity.LocateAppointmentActivity.MapActivityMode;
import static com.alberichsoft.fieldman.ui.base.PickerActivity.EXTRA_KEY_ACTION_BTN_STRING;
import static com.alberichsoft.fieldman.ui.base.PickerActivity.EXTRA_KEY_TITLE;
import static com.alberichsoft.fieldman.ui.base.PickerActivity.PickerMode;

/**
 * Created by santhanasamy_a on 7/7/2017.
 */

public class LaunchUtils {

    public static void showSignupActivity( Activity aContext ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, SignUpActivity.class);
        aContext.startActivity(lIntent);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
        aContext.finish();
    }

    public static void showLoginActivity( Activity aContext ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, LoginActivity.class);
        aContext.startActivity(lIntent);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
        aContext.finish();
    }

    public static void startHomeActivity( Activity aContext ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, HomeActivity.class);
        aContext.startActivity(lIntent);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
        aContext.finish();
    }

    public static void startMapActivityToShowTask( Activity aContext ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, LocateAppointmentActivity.class);
        lIntent.putExtra(EXTRA_KEY_LAUNCH_MODE, MapActivityMode.SHOW_APPOINTMENT.ordinal());
        aContext.startActivity(lIntent);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
    }

    public static void startMapActivityToPickLocation( Activity aContext, int aRequestCode ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, LocateAppointmentActivity.class);
        lIntent.putExtra(EXTRA_KEY_LAUNCH_MODE, MapActivityMode.PICK_LOCATION.ordinal());
        aContext.startActivityForResult(lIntent, aRequestCode);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
    }

    public static void startAppointmentPickerActivity(
            Activity aContext,
            int aTitleStringID,
            int actionBtnStringID,
            int aRequestCode ) {
        Intent lIntent = new Intent();
        lIntent.setClass(aContext, AppointmentListActivity.class);
        lIntent.putExtra(EXTRA_KEY_TITLE, aTitleStringID);
        lIntent.putExtra(EXTRA_KEY_ACTION_BTN_STRING, actionBtnStringID);
        lIntent.putExtra(PickerActivity.EXTRA_KEY_LAUNCH_MODE, PickerMode.SINGLE_PICK.ordinal());
        aContext.startActivityForResult(lIntent, aRequestCode);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
    }

    public static void startAppointmentDetailActivity(
            Activity aContext,
            Appointment aAppointment,
            AppointmentMode aMode,
            int aRequestCode ) {
        Intent lIntent = new Intent(aContext, AppointmentDetailActivity.class);
        Bundle lBundle = new Bundle();
        lBundle.putParcelable(PickerActivity.EXTRA_KEY_OBJECT, aAppointment);
        lBundle.putInt(AppointmentMode.EXTRA_KEY_LAUNCH_MODE, aMode.ordinal());
        lBundle.putInt(PickerActivity.EXTRA_KEY_TITLE, R.string.str_appointment_detail);
        lIntent.putExtras(lBundle);

        aContext.startActivityForResult(lIntent, aRequestCode);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
    }

    public static void startTechnicianPicker(
            Activity aContext,
            int aTitleStringID,
            int aRequestCode ) {
        Intent lIntent = new Intent();
        lIntent.putExtra(EXTRA_KEY_TITLE, aTitleStringID);
        lIntent.setClass(aContext, TechnicianListActivity.class);
        aContext.startActivityForResult(lIntent, aRequestCode);
        aContext.overridePendingTransition(
                R.anim.slide_right_to_left,
                R.anim.slide_inv_right_to_left);
    }

    public static void sendTechnicianPickerResult( Activity aContext, User lUser ) {

        Intent lIntent = new Intent();
        lIntent.putExtra(PickerActivity.BUNDLE_KEY_SINGLE_PICK_RESULT, lUser);
        aContext.setResult(RESULT_OK, lIntent);
        aContext.finish();
    }

}
