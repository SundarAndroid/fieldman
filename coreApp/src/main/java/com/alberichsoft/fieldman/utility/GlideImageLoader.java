
package com.alberichsoft.fieldman.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

/**
 * Image loader to load images from the Remote & Local location by using the
 * Glide interfaces. It will act as a wrapper for the interfaces provided by the
 * Glide.
 */
public class GlideImageLoader {

    public static void loadDetailScreenImage(
            final ImageView aWarningView,
            final ImageView aImageView,
            final String aUrl ) {

        Context lContext = aImageView.getContext();
        Glide.with(lContext).load(aUrl).asBitmap().centerCrop().into(
                new BitmapImageViewTarget(aImageView) {
                    @Override
                    protected void setResource( Bitmap resource ) {
                        aImageView.setImageBitmap(resource);
                        aWarningView.setVisibility(View.GONE);
                        aImageView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                        aWarningView.setVisibility(View.VISIBLE);
                        aImageView.setVisibility(View.GONE);
                    }
                });
    }

    public static void loadImage(
            final ImageView aImageView,
            final String aUrl,
            final int errorImage,
            final int placeHolder,
            final boolean aIsCircle ) {

        Context lContext = aImageView.getContext();
        Glide.with(lContext).load(aUrl).asBitmap().centerCrop().placeholder(placeHolder).into(
                new BitmapImageViewTarget(aImageView) {
                    @Override
                    protected void setResource( Bitmap resource ) {

                        if (aIsCircle) {
                            final Bitmap circleBitmap = Bitmap.createBitmap(
                                    resource.getWidth(),
                                    resource.getHeight(),
                                    Bitmap.Config.ARGB_8888);
                            final BitmapShader shader = new BitmapShader(
                                    resource,
                                    Shader.TileMode.CLAMP,
                                    Shader.TileMode.CLAMP);
                            final Paint paint = new Paint();
                            paint.setShader(shader);

                            final Canvas canvas = new Canvas(circleBitmap);
                            canvas.drawCircle(
                                    resource.getWidth() / 2,
                                    resource.getHeight() / 2,
                                    resource.getWidth() / 2,
                                    paint);
                            aImageView.setImageBitmap(circleBitmap);
                        } else {
                            aImageView.setImageBitmap(resource);
                        }
                        aImageView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadFailed( Exception e, Drawable errorDrawable ) {
                        if (-1 != errorImage) {
                            aImageView.setImageResource(errorImage);
                        } else {
                            aImageView.setVisibility(View.GONE);
                        }
                        aImageView.setContentDescription("Add Medication Image");
                    }
                });
    }

}
