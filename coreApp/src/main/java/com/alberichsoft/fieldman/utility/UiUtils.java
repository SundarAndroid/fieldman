
package com.alberichsoft.fieldman.utility;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberichsoft.fieldman.R;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.alberichsoft.common.model.AppointmentStatus.ASSIGNED;
import static com.alberichsoft.common.model.AppointmentStatus.COMPLETED;
import static com.alberichsoft.common.model.AppointmentStatus.IN_PROGRESS;
import static com.alberichsoft.common.model.AppointmentStatus.MISSED;
import static com.alberichsoft.common.model.AppointmentStatus.OPEN;
import static com.alberichsoft.common.model.AppointmentStatus.REJECTED;

/**
 * Created by santhanasamy_a on 7/9/2017.
 */

public class UiUtils {

    public static final SimpleDateFormat DATE_DISPLAY_FORMAT = new SimpleDateFormat(
            "dd/MMM/yyyy",
            Locale.US);

    public static final SimpleDateFormat DATE_EDITOR_FORMAT = new SimpleDateFormat(
            "MMM d yyyy HH:mm aaa",
            Locale.US);

    public static FullScreenDialog getDialog(
            Context aContext,
            int aStrMsgID,
            int aImageID,
            boolean aCancelable ) {

        FullScreenDialog dialog = new FullScreenDialog(aContext);
        dialog.setWarMessageTxt(aStrMsgID);
        dialog.setWarImage(aImageID);
        dialog.setCancelable(aCancelable);
        return dialog;
    }

    public static FullScreenDialog getDialog( Context aContext, int aStrMsgID, int aImageID ) {
        return getDialog(aContext, aStrMsgID, aImageID, false);
    }

    public static Dialog getAlertDialog(
            Context aContext,
            DialogInterface.OnClickListener aListener,
            String aTitle,
            String aMsg ) {
        return new AlertDialog.Builder(aContext)
                .setCancelable(false)
                .setTitle(aTitle)
                .setMessage(aMsg)
                .setPositiveButton(android.R.string.ok, aListener)
                .setNegativeButton(android.R.string.cancel, aListener)
                .create();
    }

    public static Dialog getProgressDialog( Context aContext, String aTitle, String aMsg ) {
        ProgressDialog lDialog = new ProgressDialog(aContext);
        lDialog.setTitle(aTitle);
        lDialog.setMessage(aMsg);
        lDialog.setIndeterminate(true);
        return lDialog;
    }

    public static final class FullScreenDialog extends Dialog {

        private TextView mWarTextView = null;

        private ImageView mWarImageView = null;

        private ImageView mWarProgressView = null;

        private Animation mProgressAnimation = null;

        public FullScreenDialog(@NonNull Context context) {
            this(context, android.R.style.Theme_Translucent_NoTitleBar);
        }

        public FullScreenDialog(@NonNull Context context, @StyleRes int themeResId) {
            super(context, themeResId);
            init(context);
        }

        private void init( Context aContext ) {
            mProgressAnimation = AnimationUtils.loadAnimation(aContext, R.anim.anim_progress);
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            LayoutInflater layoutInflater = (LayoutInflater) aContext
                    .getSystemService(Service.LAYOUT_INFLATER_SERVICE);
            View lView = layoutInflater.inflate(R.layout.default_dialog_layout, null);
            setContentView(lView);

            mWarTextView = (TextView) lView.findViewById(R.id.alert_warning_text);
            mWarImageView = (ImageView) lView.findViewById(R.id.alert_warning_image);
            mWarProgressView = (ImageView) lView.findViewById(R.id.alert_progress_spinner);

            mWarTextView.setText(R.string.war_loading);
            Window window = getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.dimAmount = 0.7f;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setWindowAnimations(R.style.default_dialog_animation);
            window.setAttributes(wlp);
            // (int)
            // aContext.getResources().getDimension(R.dimen.app_warning_alert_height)
            window.setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }

        public void setWarMessageTxt( int aStr ) {
            mWarTextView.setText(aStr);
        }

        public void setWarImage( int aImageID ) {
            mWarImageView.setImageResource(aImageID);
        }

        public FullScreenDialog set( int aStr, int aImageID ) {
            mWarTextView.setText(aStr);
            mWarImageView.setImageResource(aImageID);
            return this;
        }

        @Override
        public void show() {
            mWarProgressView.startAnimation(mProgressAnimation);
            super.show();
        }

        public void showProgress( int aString, int aImageID ) {
            setWarMessageTxt(aString);
            setWarImage(aImageID);
            mWarProgressView.startAnimation(mProgressAnimation);
            show();
        }

        public void showProgress( int aString ) {
            setWarMessageTxt(aString);
            // setWarImage(R.drawable.ic_list_big);
            mWarProgressView.startAnimation(mProgressAnimation);
            show();
        }
    }

    public static String[] getTaskDates() {
        Calendar lCal = Calendar.getInstance();
        Date lDate1 = lCal.getTime();
        lCal.add(Calendar.DATE, 1);
        Date lDate2 = lCal.getTime();
        lCal.add(Calendar.DATE, 1);
        Date lDate3 = lCal.getTime();

        return new String[] {
                DATE_DISPLAY_FORMAT.format(lDate1), DATE_DISPLAY_FORMAT.format(lDate2),
                DATE_DISPLAY_FORMAT.format(lDate3)
        };
    }

    public static String getDisplayableDate( long aTime ) {
        return DATE_EDITOR_FORMAT.format(aTime);
    }

    public static int getAppointmentStatusColor( int aStatusCode ) {
        int lColor = android.R.color.transparent;

        if (OPEN.ordinal() == aStatusCode) {
            lColor = R.color.task_status_open;
        } else if (ASSIGNED.ordinal() == aStatusCode) {
            lColor = R.color.task_status_assigned;
        } else if (IN_PROGRESS.ordinal() == aStatusCode) {
            lColor = R.color.task_status_in_progress;
        } else if (COMPLETED.ordinal() == aStatusCode) {
            lColor = R.color.task_status_completed;
        } else if (MISSED.ordinal() == aStatusCode) {
            lColor = R.color.task_status_missed;
        } else if (REJECTED.ordinal() == aStatusCode) {
            lColor = R.color.task_status_rejected;
        }
        return lColor;
    }

    public static int getTaskStatusString( int aStatusCode ) {

        int lStringID = R.string.str_task_status_open;
        if (OPEN.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_open;
        } else if (ASSIGNED.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_assigned;
        } else if (IN_PROGRESS.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_in_progress;
        } else if (COMPLETED.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_completed;
        } else if (MISSED.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_missed;
        } else if (REJECTED.ordinal() == aStatusCode) {
            lStringID = R.string.str_task_status_declined;
        }
        return lStringID;
    }

    public static BitmapDescriptor getMarkerDesIconFromColor( String color ) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}
