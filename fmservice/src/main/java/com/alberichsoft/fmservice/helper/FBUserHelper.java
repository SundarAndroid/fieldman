
package com.alberichsoft.fmservice.helper;

import android.support.annotation.NonNull;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.utility.CommonUtils;
import com.alberichsoft.fmservice.managers.FBUserLoader;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by santhanasamy_a on 7/9/2017.
 */

public class FBUserHelper {

    public static final String IMAGE_DIR = "users/";

    public static final String DEFAULT_PROFILE_PHOTO_URL = "https://firebasestorage.googleapis.com/v0/b/fieldman-7ef91.appspot.com/o/ic_user.png?alt=media&token=fbc54966-8f64-425c-8e90-420f8ae88e1e";

    public static void crateAppLocalUser(
            final DatabaseReference aTableRef,
            final FirebaseUser aFBUser ) {

        final User lUser = FBServiceUtils.cloneLocalUser(aFBUser);

        aTableRef.child(aFBUser.getUid()).setValue(
                lUser,
                new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(
                            DatabaseError databaseError,
                            DatabaseReference databaseReference ) {

                        if (null != databaseError) {
                            CommonUtils.printDLog(
                                    FBUserLoader.TAG,
                                    "App Specific user profile update failed!");
                        } else {
                            CommonUtils.printDLog(
                                    FBUserLoader.TAG,
                                    "App Specific user profile update success!");
                        }
                        if (FBUserLoader.IS_EMAIL_VERIFICATION_NEEDED) {
                            FBUserHelper.sendVerificationEmail(aFBUser);
                        } else {
                            FBServiceUtils.postSigUpSuccessEvent(lUser);
                        }
                    }
                });
    }

    public static void sendVerificationEmail( FirebaseUser aUser ) {
        aUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete( @NonNull Task<Void> task ) {
                if (task.isSuccessful()) {
                    FBServiceUtils.postEmailVerificationRequest();
                    CommonUtils.printDLog(FBUserLoader.TAG, "Successfully send Email Verification");
                } else {
                    CommonUtils.printDLog(FBUserLoader.TAG, "Failed to send Email Verification");
                }
            }
        });
    }
}
