
package com.alberichsoft.fmservice.managers;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by santhanasamy_a on 5/16/2017.
 */

public abstract class BaseFBLoader<D, A> implements ValueEventListener {

    protected final int FB_CLOUD_FUNCTION_EXE_THRESHOLD = 1000;

    protected DatabaseReference mDatabase = null;

    protected DatabaseReference mTableRef = null;

    protected Class mClass = null;

    protected String modelTableName;

    public abstract void onQueryAllSuccess( List<D> aDataList );

    public BaseFBLoader(Class aClass, String aQueryPath) {

        mClass = aClass;
        modelTableName = aQueryPath;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mTableRef = mDatabase.child(modelTableName);
    }

    public void queryAll() {
        mTableRef.addListenerForSingleValueEvent(this);
    }

    @Override
    public void onDataChange( DataSnapshot dataSnapshot ) {
        onQueryAllSuccess(parseSnapshot(dataSnapshot));
    }

    @Override
    public void onCancelled( DatabaseError databaseError ) {
    }

    public DatabaseReference getEndPoint() {
        return mTableRef;
    }

    public void cleanListener() {
        mTableRef.removeEventListener(this);
    }

    protected List<D> parseSnapshot( DataSnapshot dataSnapshot ) {
        Iterator<DataSnapshot> lIterator = dataSnapshot.getChildren().iterator();

        List<D> lList = new ArrayList();
        while (lIterator.hasNext()) {
            D lItem = (D) lIterator.next().getValue(mClass);
            lList.add(lItem);
        }
        return lList;
    }

}
