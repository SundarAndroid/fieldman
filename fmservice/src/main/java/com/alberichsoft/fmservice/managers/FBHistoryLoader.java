
package com.alberichsoft.fmservice.managers;

import com.alberichsoft.common.model.History;
import com.alberichsoft.common.msg.EventID;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.utility.DBUtils;

import java.util.List;

/**
 * Created by santhanasamy_a on 6/5/2017.
 */

public class FBHistoryLoader extends BaseFBLoader<History, String> {

    protected FBHistoryLoader() {
        super(History.class, DBUtils.Table.HISTORY);
    }

    @Override
    public void onQueryAllSuccess( List aDataList ) {
        new IEvent.Builder()
                .setEventID(EventID.IHistory.GET_HISTORY_SUCCESS)
                .setEventName("History loaded from FB : Notify")
                .setEventObject(aDataList)
                .build()
                .post();
    }
}
