
package com.alberichsoft.fmservice.managers;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.AppointmentStatus;
import com.alberichsoft.common.model.Customer;
import com.alberichsoft.common.model.User;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.alberichsoft.fmservice.util.FBUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.alberichsoft.common.utility.DBUtils.AppointmentField;
import static com.alberichsoft.common.utility.DBUtils.Table;

/**
 *
 */
public class FBAppointmentLoader extends BaseFBLoader<Appointment, String> {

    public static final String TAG = FBAppointmentLoader.class.getSimpleName();

    private ValueEventListener mSingleValueListener = null;

    private ValueEventListener mAllValueListener = null;

    protected FBAppointmentLoader() {
        super(Appointment.class, Table.APPOINTMENT);

        mAllValueListener = new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                FBUtils.prepareAppointmentList(mDatabase, parseSnapshot(dataSnapshot));
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        };

        mSingleValueListener = new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                Appointment lAppointment = dataSnapshot.getValue(Appointment.class);
                FBServiceUtils.postAppointmentUpdateEvent(lAppointment);
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        };

    }

    @Override
    public void onQueryAllSuccess( List<Appointment> aAppointmentList ) {

    }

    public void createNew( Appointment appointment ) {

        Map<String, Object> update = new HashMap<>();

        String lCustomerKey = mDatabase.child(Table.CUSTOMER).push().getKey();
        String lAppointmentKey = mDatabase.child(Table.APPOINTMENT).push().getKey();

        appointment.setStatus(AppointmentStatus.OPEN.ordinal());
        Customer lCustomer = appointment.getCustomer();
        lCustomer.setCustomerID(lCustomerKey);

        appointment.setCustomerID(lCustomerKey);
        appointment.setAppointmentID(lAppointmentKey);

        update.put(Table.CUSTOMER + "/" + lCustomerKey, lCustomer);
        update.put(Table.APPOINTMENT + "/" + lAppointmentKey, appointment);

        mDatabase.updateChildren(update).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete( @NonNull Task<Void> task ) {
                if (task.isSuccessful())
                    FBServiceUtils.postCreateAppointmentSuccess();
                else {
                    FBServiceUtils.postCreateAppointmentFailure();
                }
            }
        });

    }

    /**
     * Method to get the assigned task for Today, Tomorrow, and Day after
     * tomorrow
     *
     * @param aUserID
     */
    public void getCurrentUserAppointment( final User aUserID ) {

        if (null == aUserID) {
            FBServiceUtils.postGetCurrentUserAppointmentFailed("User is empty");
            return;
        }

        long lStatTime = FBServiceUtils.getStartOfTheDay();
        long lEndTime = FBServiceUtils.getEndOfTheDayFromToday(3);

        mTableRef
                .orderByChild(AppointmentField.TECHNICIAN_ID)
                // .startAt(lStatTime, AppointmentField.ASSIGNED_TIME)
                .equalTo(aUserID.getUserID())
                .addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {

                        FBUtils.prepareUserAppointmentList(mDatabase, parseSnapshot(dataSnapshot));
                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {

                        FBServiceUtils
                                .postGetCurrentUserAppointmentFailed(databaseError.getMessage());
                    }
                });
    }

    /**
     *
     */
    public void getAllAppointments( final AppointmentStatus aStatus ) {

        long lStatTime = FBServiceUtils.getStartOfTheDay();
        long lEndTime = FBServiceUtils.getEndOfTheDayFromToday(3);

        Query lRef = mTableRef.orderByChild(AppointmentField.STATUS);
        if (null != aStatus) {
            lRef = lRef.equalTo(aStatus.ordinal());
        }
        // .startAt(lStatTime, AppointmentField.ASSIGNED_TIME)
        lRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {

                final List<Appointment> lAppointmentList = parseSnapshot(dataSnapshot);

                if (null == lAppointmentList || lAppointmentList.isEmpty()) {
                    FBServiceUtils.postGetAppointmentSuccess(new ArrayList<Appointment>(0));
                    return;
                }

                final Map<String, Appointment> lUserIDAppointmentMap = new HashMap<>();
                for (Appointment appointment : lAppointmentList) {
                    lUserIDAppointmentMap.put(appointment.getCustomerID(), appointment);
                }
                mDatabase.child(Table.CUSTOMER).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange( DataSnapshot dataSnapshot ) {

                                final List<Customer> lList = FBServiceUtils
                                        .parseSnapshot(dataSnapshot, Customer.class);

                                for (Customer customer : lList) {
                                    String lCustomerID = customer.getCustomerID();
                                    if (lUserIDAppointmentMap.containsKey(lCustomerID)) {
                                        lUserIDAppointmentMap
                                                .get(lCustomerID)
                                                .setCustomer(customer);
                                    }
                                }
                                FBServiceUtils.postGetAppointmentSuccess(lAppointmentList);
                            }

                            @Override
                            public void onCancelled( DatabaseError databaseError ) {

                                FBServiceUtils.postGetAppointmentFailed(databaseError.getMessage());
                            }
                        });

            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

                FBServiceUtils.postGetAppointmentFailed(databaseError.getMessage());
            }
        });
    }

    public void assignAppointment( final Appointment appointment ) {

        HashMap<String, Object> lUpdateMap = new HashMap<>();
        lUpdateMap.put(AppointmentField.TECHNICIAN_ID, appointment.getTechnicianID());
        lUpdateMap.put(AppointmentField.STATUS, AppointmentStatus.ASSIGNED.ordinal());

        mTableRef
                .child(appointment.getAppointmentID())
                .updateChildren(lUpdateMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( @NonNull Task<Void> task ) {
                        if (task.isSuccessful()) {
                            FBServiceUtils.postAssignTechnicianSuccess(
                                    "Appointment fix with the technician"
                                            + appointment.getAppointmentID() + ","
                                            + appointment.getTechnicianID());
                        } else {
                            FBServiceUtils.postAssignTechnicianFailed(task.getException());
                        }

                    }
                });
    }

    public void startAppointment( final Appointment appointment ) {

        HashMap<String, Object> lUpdateMap = new HashMap<>();
        lUpdateMap.put(AppointmentField.START_TIME, "" + new Date().getTime());
        lUpdateMap.put(AppointmentField.STATUS, AppointmentStatus.IN_PROGRESS.ordinal());

        mTableRef
                .child(appointment.getAppointmentID())
                .updateChildren(lUpdateMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( @NonNull Task<Void> task ) {
                        if (task.isSuccessful()) {
                            FBServiceUtils.postStartTaskSuccessEvent();
                        } else {
                            FBServiceUtils.postStartTaskFailedEvent(task.getException());
                        }
                    }
                });
    }

    public void finishAppointment( final Appointment appointment ) {

        HashMap<String, Object> lUpdateMap = new HashMap<>();
        lUpdateMap.put(AppointmentField.END_TIME, "" + new Date().getTime());
        lUpdateMap.put(AppointmentField.STATUS, AppointmentStatus.COMPLETED.ordinal());

        mTableRef
                .child(appointment.getAppointmentID())
                .updateChildren(lUpdateMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( @NonNull Task<Void> task ) {
                        if (task.isSuccessful()) {
                            FBServiceUtils.postFinishTaskSuccessEvent();
                        } else {
                            FBServiceUtils.postFinishTaskFailedEvent(task.getException());
                        }
                    }
                });
    }

    public void getAppointmentByID( String appointmentID ) {
        mDatabase.child(Table.APPOINTMENT).child(appointmentID).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {

                        Appointment lAppointment = dataSnapshot.getValue(Appointment.class);
                        FBServiceUtils.postGetAppointmentByIDSuccess(lAppointment);
                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {

                        FBServiceUtils.postGetAppointmentByIDFailed(databaseError.getMessage());
                    }
                });
    }

    public void addRootValueListener() {

        mTableRef.addValueEventListener(mAllValueListener);
    }

    public void removeRootValueListener() {
        mTableRef.removeEventListener(mAllValueListener);
    }

    private String mAppointIDToListen = "";

    public void addSingleValueListener( String appointmentID ) {

        if (!TextUtils.isEmpty(mAppointIDToListen)) {
            removeSingleValueListener(mAppointIDToListen);
        }

        mAppointIDToListen = appointmentID;
        mTableRef.child(mAppointIDToListen).addValueEventListener(mSingleValueListener);
    }

    public void removeSingleValueListener( String appointmentID ) {

        mTableRef.child(appointmentID).removeEventListener(mSingleValueListener);
        mAppointIDToListen = "";

    }
}
