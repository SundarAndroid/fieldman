
package com.alberichsoft.fmservice.managers;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.alberichsoft.common.model.User;
import com.alberichsoft.common.utility.CommonUtils;
import com.alberichsoft.fmservice.helper.FBUserHelper;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuth.AuthStateListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.alberichsoft.common.msg.EventID.IUser;
import static com.alberichsoft.common.msg.IEvent.Builder;
import static com.alberichsoft.common.utility.DBUtils.Table;
import static com.alberichsoft.common.utility.DBUtils.UserField;

/**
 * Created by santhanasamy_a on 10/20/2016.
 */

public class FBUserLoader extends BaseFBLoader<User, String> implements AuthStateListener {

    public static final String TAG = FBUserLoader.class.getSimpleName();

    public static final boolean IS_EMAIL_VERIFICATION_NEEDED = true;

    private ValueEventListener mAppLocalUserVEListener = null;

    private FirebaseAuth mFBAuthenticator = null;

    private User mUser = null;

    protected FBUserLoader() {
        super(User.class, Table.USERS);
        mFBAuthenticator = FirebaseAuth.getInstance();

        mAppLocalUserVEListener = new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {

                mUser = dataSnapshot.getValue(User.class);

                if (null != mUser) {
                    FBServiceUtils.postAppUserDataChangeEvent(mUser);
                }
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        };
    }

    @Override
    public void onAuthStateChanged( @NonNull FirebaseAuth firebaseAuth ) {

        FirebaseUser lFBUser = firebaseAuth.getCurrentUser();
        FBServiceUtils.postAuthStateChangeEvent((null != lFBUser));
        if (null != lFBUser) {
            getAppLocalUser(lFBUser);
        }
    }

    public void signIn( String email, String password ) {

        mFBAuthenticator.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete( @NonNull Task<AuthResult> task ) {

                        if (task.isSuccessful()) {
                            FirebaseUser lUser = task.getResult().getUser();
                            // getAppLocalUser(lUser);
                            addListener();
                        } else {
                            FBServiceUtils.postSigInFailedEvent(task.getException());
                        }
                    }
                });
    }

    public void getAppLocalUser( FirebaseUser aUser ) {
        mTableRef.child(aUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                mUser = dataSnapshot.getValue(User.class);
                FBServiceUtils.postSigInSuccessEvent(mUser);
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {
                FBServiceUtils.postSigInFailedEvent(new Exception(databaseError.getMessage()));
            }
        });
    }

    public void reloadAppLocalUser( FirebaseUser aUser ) {
        mTableRef.child(aUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                mUser = dataSnapshot.getValue(User.class);
                FBServiceUtils.postGetUserEventSuccess(mUser);
            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {
                FBServiceUtils.postGetUserEventFailed(databaseError.getMessage());
            }
        });
    }

    public void signUp( final String aDisplayName, String aEmail, String aPassword ) {

        CommonUtils.printILog(TAG, "Sig-up", aEmail);

        mFBAuthenticator.createUserWithEmailAndPassword(aEmail, aPassword).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete( @NonNull Task<AuthResult> task ) {

                        if (task.isSuccessful()) {
                            FirebaseUser lFBUser = task.getResult().getUser();
                            upDateFBUserProfile(lFBUser, aDisplayName);
                        } else {
                            FBServiceUtils.postSigUpFailedEvent(task.getException());
                        }
                    }
                });
    }

    private void upDateFBUserProfile( final FirebaseUser aFBUser, String aDisplayName ) {

        // 1. Update FBUser
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(aDisplayName)
                .setPhotoUri(Uri.parse(FBUserHelper.DEFAULT_PROFILE_PHOTO_URL))
                .build();

        aFBUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete( @NonNull Task<Void> task ) {
                if (task.isSuccessful()) {
                    CommonUtils.printDLog(TAG, "FB-User profile update : Success");
                    // 2. Update App Local User
                    FBUserHelper.crateAppLocalUser(mTableRef, aFBUser);

                    // 3. Add listener for the User info update
                    mTableRef
                            .child(aFBUser.getUid())
                            .addValueEventListener(mAppLocalUserVEListener);
                } else {
                    CommonUtils.printDLog(TAG, "FB-User profile update : Failed");
                    FBServiceUtils.postSigUpFailedEvent(task.getException());
                }
            }
        });
    }

    public void signOut() {

        if (null == mFBAuthenticator) {
            return;
        }
        cleanListener();
    }

    public boolean isAuthenticated() {
        return null != mFBAuthenticator && null != mFBAuthenticator.getCurrentUser();
    }

    public FirebaseUser getFBUser() {
        if (null != mFBAuthenticator) {
            return mFBAuthenticator.getCurrentUser();
        }

        return null;
    }

    public User getUser() {
        return mUser;
    }

    @Override
    public void cleanListener() {

        FirebaseUser lUser = mFBAuthenticator.getCurrentUser();
        if (null == lUser) {
            FBServiceUtils.postAuthStateChangeEvent(false);
            return;
        }

        // 1. Clean current user's end point
        DatabaseReference lUserEndPoint = mTableRef.child(lUser.getUid());
        lUserEndPoint.keepSynced(false);
        lUserEndPoint.removeEventListener(this);

        // 2. Clean User table end point listener
        mTableRef.removeEventListener(this);
        mFBAuthenticator.signOut();
        mUser = null;

        // 3. Remove Auth state change listener
        // mFBAuthenticator.removeAuthStateListener(this);
    }

    public void addListener() {

        if (null == mFBAuthenticator) {
            return;
        }

        FirebaseUser lUser = mFBAuthenticator.getCurrentUser();
        if (null == lUser) {
            return;
        }

        // 1. Add current user's end point
        DatabaseReference lUserEndPoint = mTableRef.child(lUser.getUid());
        lUserEndPoint.addValueEventListener(mAppLocalUserVEListener);
        lUserEndPoint.keepSynced(true);

        // 2. Add User table end point listener
        mTableRef.addValueEventListener(this);

        // 3. Add Auth state change listener
        mFBAuthenticator.addAuthStateListener(FBUserLoader.this);
    }

    public static void getUserToken() {
        FirebaseUser lUser = FirebaseAuth.getInstance().getCurrentUser();
        lUser.getToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            public void onComplete( @NonNull Task<GetTokenResult> task ) {
                if (task.isSuccessful()) {
                    String idToken = task.getResult().getToken();
                    // Send token to your backend via HTTPS
                    CommonUtils.printDLog(TAG, "Refresh Token" + idToken);
                } else {
                    CommonUtils.printDLog(TAG, "Refresh Token -> Failed" + task.getException());
                }
            }
        });
    }

    public void getUserById( final String aUserID ) {

        DatabaseReference lUserTableRef = mTableRef.child(aUserID);
        lUserTableRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange( DataSnapshot dataSnapshot ) {
                User lUser = dataSnapshot.getValue(User.class);
                if (null == lUser) {
                    return;
                }
                new Builder()
                        .setEventID(IUser.GET_USER_BY_ID_SUCCESS)

                        .setEventName(
                                "Found user from the supplied ID :- " + aUserID + " , "
                                        + lUser.getUserName())
                        .setEventObject(lUser)
                        .build()
                        .post();

            }

            @Override
            public void onCancelled( DatabaseError databaseError ) {

            }
        });
    }

    @Override
    public void onQueryAllSuccess( List<User> aDataList ) {
        // Notify
        new Builder()
                .setEventID(IUser.GET_USER_LIST_SUCCESS)
                .setEventName("User List loaded from FB : Notify")
                .setEventObject(aDataList)
                .build()
                .post();
    }

    public void getTechnicianList() {
        mTableRef.orderByChild(UserField.USER_ROLE).equalTo("2").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {

                        Iterator<DataSnapshot> lIterator = dataSnapshot.getChildren().iterator();

                        List<User> lList = new ArrayList();
                        while (lIterator.hasNext()) {
                            User lItem = (User) lIterator.next().getValue(mClass);
                            lList.add(lItem);
                        }
                        FBServiceUtils.postQueryTechnicianListSuccess(lList);
                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {
                        FBServiceUtils.postQueryTechnicianListFailed(databaseError.getMessage());
                    }
                });
    }

    public void saveUserImage( final User aUser ) {

        if (aUser == null || TextUtils.isEmpty(aUser.getImageUrl())) {
            return;
        }

        Uri fileUri = Uri.parse(aUser.getImageUrl());
        if (null == fileUri) {
            return;
        }

        StorageReference lStorageReference = FirebaseStorage.getInstance().getReference();
        StorageReference lStoragePath = lStorageReference.child(
                FBUserHelper.IMAGE_DIR + File.separator + aUser.getUserID() + File.separator
                        + fileUri.getLastPathSegment());
        lStoragePath
                .putFile(fileUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess( UploadTask.TaskSnapshot taskSnapshot ) {

                        Uri uri = taskSnapshot.getDownloadUrl();
                        saveUserImageURI(aUser, uri);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( @NonNull Exception e ) {

                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress( UploadTask.TaskSnapshot taskSnapshot ) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred())
                                / taskSnapshot.getTotalByteCount();
                        new Builder()
                                .setEventID(IUser.SAVE_USER_IMAGE_PROGRESS)
                                .setEventName("Saving user image in progress ! " + progress)
                                .setEventObject(progress)
                                .build()
                                .post();

                    }
                });
    }

    private void saveUserImageURI( User aUser, Uri aUri ) {
        mTableRef.child(aUser.getUserID()).child(UserField.USER_IMAGE_URL).setValue(
                aUri,
                new DatabaseReference.CompletionListener() {

                    @Override
                    public void onComplete(
                            DatabaseError databaseError,
                            DatabaseReference databaseReference ) {

                        if (null == databaseError) {
                            new Builder()
                                    .setEventID(IUser.SAVE_USER_IMAGE_SUCCESS)
                                    .setEventName("Saving user image success!")
                                    .build()
                                    .post();
                        } else {
                            new Builder()
                                    .setEventID(IUser.SAVE_USER_IMAGE_FAILED)
                                    .setEventName("Saving user image failed!")
                                    .build()
                                    .post();
                        }
                    }
                });
    }

}
