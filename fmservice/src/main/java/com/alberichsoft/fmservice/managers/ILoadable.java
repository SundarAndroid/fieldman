
package com.alberichsoft.fmservice.managers;

/**
 * Created by santhanasamy_a on 7/12/2017.
 */

public interface ILoadable<D, A> {
    void createNew(D aData );

    void update( D aData, A... arg );

    void edit( D aData );

    void delete( D aData );

    void query( String[] aWhere, String[] aValue );

    void queryAll();

}
