
package com.alberichsoft.fmservice.managers;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.AppointmentStatus;
import com.alberichsoft.common.model.User;
import com.alberichsoft.common.msg.IServiceEvent;
import com.alberichsoft.fmservice.util.FBServiceUtils;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;

import static com.alberichsoft.common.msg.EventID.IAppointment;
import static com.alberichsoft.common.msg.EventID.IHistory;
import static com.alberichsoft.common.msg.EventID.ILogin;
import static com.alberichsoft.common.msg.EventID.IUser;
import static com.alberichsoft.common.utility.DBUtils.Table;

/**
 * 
 */

public class FBManager {

    private static final String TAG = FBManager.class.getSimpleName();

    private static FBManager sManager = null;

    private HashMap<String, BaseFBLoader> mLoaderMap = null;

    private FBManager() {
        mLoaderMap = new HashMap<>();
    }

    public static FBManager getInstance() {

        if (null == sManager) {
            synchronized (FBManager.class) {
                if (null == sManager) {
                    sManager = new FBManager();
                }
            }
        }
        return sManager;
    }

    protected BaseFBLoader getLoader( String aKey ) {

        if (null == aKey) {
            System.out.println("[Loader Name shouldn't be null] Supplied Key : [" + aKey + "]");
            return null;
        }
        BaseFBLoader lLoader = mLoaderMap.get(aKey);
        if (null != lLoader) {
            return lLoader;
        }

        switch (aKey) {
            case Table.USERS: {
                lLoader = new FBUserLoader();
                break;
            }
            case Table.CUSTOMER: {
                // lLoader = new FBDeviceLoader();
                break;
            }
            case Table.HISTORY: {
                lLoader = new FBHistoryLoader();
                break;
            }
            case Table.APPOINTMENT: {
                lLoader = new FBAppointmentLoader();
                break;
            }
        }

        mLoaderMap.put(aKey, lLoader);
        return lLoader;
    }

    public DatabaseReference getEndPoint( String aKey ) {
        return getLoader(aKey).getEndPoint();
    }

    public void onEvent( IServiceEvent aEvent ) {

        int lID = aEvent.getEventID();
        FBUserLoader lUserLoader = (FBUserLoader) getLoader(Table.USERS);
        FBHistoryLoader lHistoryLoader = (FBHistoryLoader) getLoader(Table.HISTORY);
        FBAppointmentLoader lAppointmentLoader = (FBAppointmentLoader) getLoader(Table.APPOINTMENT);

        switch (lID) {

            case ILogin.IS_AUTHENTICATED: {
                FBServiceUtils.getAuthStateEvent(lUserLoader.isAuthenticated()).post();
                break;
            }
            case ILogin.SIGN_IN: {
                User lUser = (User) aEvent.getEventObject();
                lUserLoader.signIn(lUser.getEmail(), lUser.getPassword());
                break;
            }
            case ILogin.SIGN_UP: {
                User lUser = (User) aEvent.getEventObject();
                lUserLoader.signUp(lUser.getUserName(), lUser.getEmail(), lUser.getPassword());
                break;
            }
            case ILogin.SIGN_OUT: {
                lUserLoader.signOut();
                break;
            }
            // User
            case IUser.GET_CURRENT_USER: {

                User lUser = lUserLoader.getUser();
                FirebaseUser lFBUser = lUserLoader.getFBUser();
                if (null == lUser && null != lFBUser) {
                    lUserLoader.reloadAppLocalUser(lFBUser);
                } else {
                    if (null != lUser) {
                        FBServiceUtils.postGetUserEventSuccess(lUser);
                    } else {
                        FBServiceUtils.postGetUserEventFailed(
                                ".GET_CURRENT_USER -> Failed, Please login");
                    }
                }
                break;
            }
            case IUser.GET_USER_BY_ID: {

                lUserLoader.getUserById((String) aEvent.getEventObject());
                break;
            }
            case IUser.GET_USER_LIST: {
                lUserLoader.queryAll();
                break;
            }
            case IUser.SAVE_USER_IMAGE: {
                User lUser = (User) aEvent.getEventObject();
                lUserLoader.saveUserImage(lUser);
                break;
            }
            case IUser.GET_TECHNICIAN_LIST: {
                lUserLoader.getTechnicianList();
                break;
            }
            // Appointment
            case IAppointment.CREATE_APPOINTMENT: {
                lAppointmentLoader.createNew((Appointment) aEvent.getEventObject());
                break;
            }
            case IAppointment.GET_USER_APPOINTMENTS: {
                lAppointmentLoader.getCurrentUserAppointment(lUserLoader.getUser());
                break;
            }
            case IAppointment.GET_ALL_APPOINTMENTS: {
                AppointmentStatus lStatus = (AppointmentStatus) aEvent.getEventObject();
                lAppointmentLoader.getAllAppointments(lStatus);
                break;
            }
            case IAppointment.ASSIGN_APPOINTMENT: {
                lAppointmentLoader.assignAppointment((Appointment) aEvent.getEventObject());
                break;
            }
            case IAppointment.START_APPOINTMENT: {
                lAppointmentLoader.startAppointment((Appointment) aEvent.getEventObject());
                break;
            }
            case IAppointment.FINISH_APPOINTMENT: {
                lAppointmentLoader.finishAppointment((Appointment) aEvent.getEventObject());
                break;
            }
            case IAppointment.GET_APPOINTMENT_BY_ID: {
                lAppointmentLoader.getAppointmentByID((String) aEvent.getEventObject());
                break;
            }
            case IAppointment.ADD_ALL_VALUE_LISTENER: {
                lAppointmentLoader.addRootValueListener();
                break;
            }
            case IAppointment.REMOVE_ALL_VALUE_LISTENER: {
                lAppointmentLoader.removeRootValueListener();
                break;
            }
            case IAppointment.ADD_SINGLE_VALUE_LISTENER: {
                lAppointmentLoader.addSingleValueListener((String) aEvent.getEventObject());
                break;
            }

            case IAppointment.REMOVE_SINGLE_VALUE_LISTENER: {
                lAppointmentLoader.removeSingleValueListener((String) aEvent.getEventObject());
                break;
            }

            // History
            case IHistory.GET_HISTORY_LIST: {
                lHistoryLoader.queryAll();
                break;
            }

        }
    }

    public void clean() {
        mLoaderMap.clear();
        mLoaderMap = null;
    }
}
