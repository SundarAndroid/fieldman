
package com.alberichsoft.fmservice.fire.core;

import android.app.Application;

import com.alberichsoft.fmservice.fire.utils.FBConstants;
import com.alberichsoft.fmservice.fire.utils.ResourceLoader;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * TODO : This call is intent for the server side implementation. Wrongly added
 * here
 */

public class FBInitializer {

    private static FBInitializer instance;

    public static FBInitializer getInstance() {

        if (null == instance) {

            synchronized (FBInitializer.class) {

                if (null == instance) {
                    instance = new FBInitializer();
                }
            }
        }
        return instance;
    }

    public void initialize( Application app ) {
        FirebaseApp.initializeApp(app, FirebaseOptions.fromResource(app), FBConstants.FB_APP_NAME);
    }

    private void FBInitializer() throws FileNotFoundException {

        InputStream lIStream = ResourceLoader.load(getClass(), FBConstants.FB_CONFIG_FILE_PATH);

        FirebaseOptions options = new FirebaseOptions.Builder()
                // .setServiceAccount(lIStream)
                .setDatabaseUrl(FBConstants.FB_DB_PATH).build();

        // FirebaseApp.initializeApp(options);
    }

    public String getToken( String uid ) {
        return null;// FirebaseAuth.getInstance().createCustomToken(uid);
    }

    public static void subscribe() {
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }
}
