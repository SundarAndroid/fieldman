
package com.alberichsoft.fmservice.fire.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.alberichsoft.common.utility.CommonUtils;

/**
 * Created by santhanasamy_a on 10/13/2016.
 */

public class ResourceLoader {

    public static InputStream load( Class lClass, String fileName ) throws FileNotFoundException {

        CommonUtils.printDLog("Reading prop file", fileName);

        // 1.
        InputStream lStream = lClass.getResourceAsStream(fileName);

        if (null != lStream) {
            return lStream;
        }

        // 2.
        lStream = ClassLoader.getSystemClassLoader().getResourceAsStream(fileName);
        if (null != lStream) {
            return lStream;
        }

        // 3.
        ClassLoader classLoader = lClass.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        if (null != file) {
            return new FileInputStream(file);
        }

        return null;
    }

}
