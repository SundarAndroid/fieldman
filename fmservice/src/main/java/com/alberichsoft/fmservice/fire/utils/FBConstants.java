
package com.alberichsoft.fmservice.fire.utils;

/**
 * Created by santhanasamy_a on 10/20/2016.
 */

public class FBConstants {

    public static final String FB_APP_NAME = "FieldMan";

    public static final String URL_PATH_DIVIDER = "/";

    public static final String CONTENT_TYPE = "application/json";

    public static final String FB_CONFIG_FILE_NAME = URL_PATH_DIVIDER + "MedicalConference.json";

    public static final String FB_CONFIG_FILE_PATH = URL_PATH_DIVIDER + FB_CONFIG_FILE_NAME;

    public static final String SERVER_AUTH_KEY = "AIzaSyAjAIuwaKwkGIDZ9ETZhsxCT20d69r68Y0";

    public static final String FB_DB_PATH = "https://medicalconference-6453b.firebaseio.com/";

}
