
package com.alberichsoft.fmservice.util;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.Customer;
import com.alberichsoft.common.utility.DBUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by santhanasamy_a on 7/19/2017.
 */

public class FBUtils {

    public static void prepareAppointmentList(
            DatabaseReference aReference,
            final List<Appointment> appointmentList ) {

        aReference.child(DBUtils.Table.CUSTOMER).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {

                        final List<Customer> lList = FBServiceUtils
                                .parseSnapshot(dataSnapshot, Customer.class);

                        final Map<String, Customer> lCustomerIDMap = new HashMap<>();
                        for (Customer customer : lList) {
                            lCustomerIDMap.put(customer.getCustomerID(), customer);
                        }
                        for (Appointment appointment : appointmentList) {
                            Customer lMatchingCustomer = lCustomerIDMap
                                    .get(appointment.getCustomerID());
                            appointment.setCustomer(lMatchingCustomer);
                        }
                        FBServiceUtils.postGetAppointmentSuccess(appointmentList);
                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {

                        FBServiceUtils.postGetAppointmentFailed(databaseError.getMessage());
                    }
                });
    }

    public static void prepareUserAppointmentList(
            DatabaseReference aReference,
            final List<Appointment> appointmentList ) {

        final Map<String, Appointment> lUserIDAppointmentMap = new HashMap<>();
        for (Appointment appointment : appointmentList) {
            lUserIDAppointmentMap.put(appointment.getCustomerID(), appointment);
        }
        aReference.child(DBUtils.Table.CUSTOMER).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot ) {

                        final List<Customer> lList = FBServiceUtils
                                .parseSnapshot(dataSnapshot, Customer.class);

                        final Map<String, Customer> lTempMap = new HashMap<>();

                        for (Customer customer : lList) {
                            lTempMap.put(customer.getCustomerID(), customer);

                            String lCustomerID = customer.getCustomerID();
                            if (lUserIDAppointmentMap.containsKey(lCustomerID)) {
                                lUserIDAppointmentMap.get(lCustomerID).setCustomer(customer);
                            }
                        }
                        FBServiceUtils.postGetCurrentUserAppointmentSuccess(appointmentList);
                    }

                    @Override
                    public void onCancelled( DatabaseError databaseError ) {

                        FBServiceUtils
                                .postGetCurrentUserAppointmentFailed(databaseError.getMessage());
                    }
                });

    }
}
