
package com.alberichsoft.fmservice.util;

import com.alberichsoft.common.model.Appointment;
import com.alberichsoft.common.model.User;
import com.alberichsoft.common.model.UserRole;
import com.alberichsoft.common.msg.IEvent;
import com.alberichsoft.common.msg.IMessage;
import com.alberichsoft.common.msg.IServiceEvent;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.alberichsoft.common.msg.EventID.IAppointment;
import static com.alberichsoft.common.msg.EventID.ILogin;
import static com.alberichsoft.common.msg.EventID.IUser;

/**
 * Created by santhanasamy_a on 5/24/2017.
 */

public class FBServiceUtils {

    public static final SimpleDateFormat DATE_SERVER_FORMAT = new SimpleDateFormat(
            "dd MMM yyyy",
            Locale.US);

    public static List parseSnapshot( DataSnapshot dataSnapshot, Class aClass ) {
        Iterator<DataSnapshot> lIterator = dataSnapshot.getChildren().iterator();

        List lList = new ArrayList();
        while (lIterator.hasNext()) {
            lList.add(lIterator.next().getValue(aClass));
        }
        return lList;
    }

    public static long getStartOfTheDay() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime().getTime();
    }

    public static long getEndOfTheDayFromToday( int aNoOfDays ) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, aNoOfDays);
        return calendar.getTime().getTime();
    }

    // Get Current User
    public static void postGetUserEvent() {
        new IServiceEvent.Builder()
                .setEventID(IUser.GET_CURRENT_USER)
                .setEventName("Get user from FB Remote")
                .build()
                .post();
    }

    public static void postGetUserEventSuccess( User aUser ) {
        new IEvent.Builder()
                .setEventID(IUser.GET_CURRENT_USER_SUCCESS)
                .setEventObject(aUser)
                .setEventName("Get user from User-Manager -> Success")
                .build()
                .post();
    }

    public static void postGetUserEventFailed( String aExceptionMsg ) {
        new IEvent.Builder()
                .setEventID(IUser.GET_CURRENT_USER_FAILED)
                .setEventObject(aExceptionMsg)
                .setEventName("Get user from User-Manager -> Failed")
                .build()
                .post();
    }

    public static IMessage getSigInEvent( String aEmail, String aPassword ) {
        User lTempUser = new User();
        lTempUser.setEmail(aEmail);
        lTempUser.setPassword(aPassword);
        return new IServiceEvent.Builder()
                .setEventID(ILogin.SIGN_IN)
                .setEventName("Sign-In Request for Email&Password")
                .setEventObject(lTempUser)
                .build();
    }

    public static IMessage getSigUpEvent( String aDisplayName, String aEmail, String aPassword ) {
        User lTempUser = new User();
        lTempUser.setUserName(aDisplayName);
        lTempUser.setEmail(aEmail);
        lTempUser.setPassword(aPassword);
        return new IServiceEvent.Builder()
                .setEventID(ILogin.SIGN_UP)
                .setEventName("Sign-Up Request for Email&Password")
                .setEventObject(lTempUser)
                .build();
    }

    public static IMessage getSigOutEvent() {
        return new IServiceEvent.Builder()
                .setEventID(ILogin.SIGN_OUT)
                .setEventName("Sign-Out requested")
                .build();
    }

    public static void postAppUserDataChangeEvent( User aUser ) {
        new IEvent.Builder()
                .setEventID(IUser.CURRENT_USER_DATA_CHANGED)
                .setEventName("App User data changed : User Name :- " + aUser.getUserName())
                .setEventObject(aUser)
                .build()
                .post();
    }

    public static void postSigUpSuccessEvent( User aUser ) {

        new IEvent.Builder()
                .setEventID(ILogin.SIGN_UP_SUCCESS)
                .setEventObject(aUser)
                .setEventName("signUp - WithEmailAndPassword:onComplete")
                .build()
                .post();
    }

    public static void postSigUpFailedEvent( Exception aException ) {

        new IEvent.Builder()
                .setEventID(ILogin.SIGN_UP_FAILED)
                .setEventObject(aException)
                .setEventName("signUP - WithEmailAndPassword:Failed")
                .build()
                .post();
    }

    public static void postSigInSuccessEvent( User aUser ) {

        new IEvent.Builder()
                .setEventID(ILogin.SIGN_IN_SUCCESS)
                .setEventObject(aUser)
                .setEventName("signIn - WithEmailAndPassword:Success")
                .build()
                .post();
    }

    public static void postSigInFailedEvent( Exception aException ) {

        new IEvent.Builder()
                .setEventID(ILogin.SIGN_IN_FAILED)
                .setEventObject(aException)
                .setEventName("signIn - WithEmailAndPassword:Failed")
                .build()
                .post();
    }

    public static IMessage getAuthStateEvent( boolean isAuthenticated ) {
        return new IEvent.Builder()
                .setEventID(ILogin.IS_AUTHENTICATED)
                .setEventName("Is Authenticated ? " + isAuthenticated)
                .setEventObject(isAuthenticated)
                .build();
    }

    public static void postAuthStateChangeEvent( boolean isAuthenticated ) {

        new IEvent.Builder()
                .setEventID(
                        isAuthenticated ? ILogin.AUTHENTICATION_SUCCESS
                                : ILogin.AUTHENTICATION_FAILED)
                .setEventName("onAuthStateChanged :[Is Authenticated][" + isAuthenticated + "]")
                .build()
                .postSticky();
    }

    public static void postEmailVerificationRequest() {
        new IEvent.Builder()
                .setEventID(ILogin.REQUEST_EMAIL_VERIFICATION)
                .setEventName("Requesting email verification !")
                .build()
                .postSticky();

    }

    private static final User sUser = new User();

    public static User cloneLocalUser( FirebaseUser aFBUser ) {

        if (null == aFBUser) {
            sUser.setUserName("");
            sUser.setUserID("");
            sUser.setEmail("");
            sUser.setImageUrl("");
            sUser.setVerified(false);
            return sUser;
        }

        sUser.setUserID(aFBUser.getUid());
        sUser.setEmail(aFBUser.getEmail());
        sUser.setVerified(aFBUser.isEmailVerified());
        sUser.setRole("" + UserRole.TECHNICIAN.ordinal());
        sUser.setImageUrl(null != aFBUser.getPhotoUrl() ? aFBUser.getPhotoUrl().toString() : "");
        sUser.setUserName(aFBUser.getDisplayName());
        return sUser;
    }

    public static IMessage getShowLoginRequest() {

        return new IEvent.Builder()
                .setEventID(ILogin.SHOW_LOGIN)
                .setEventName("Request to show Login Screen")
                .build();
    }

    public static void postCreateAppointmentEvent( Appointment appointment ) {

        new IServiceEvent.Builder()
                .setEventID(IAppointment.CREATE_APPOINTMENT)
                .setEventName("Request to create Appointment!")
                .setEventObject(appointment)
                .build()
                .post();
    }

    public static void postCreateAppointmentSuccess() {

        new IEvent.Builder()
                .setEventID(IAppointment.CREATE_APPOINTMENT_SUCCESS)
                .setEventName("Create Appointment : Success")
                .build()
                .post();
    }

    public static void postCreateAppointmentFailure() {

        new IEvent.Builder()
                .setEventID(IAppointment.CREATE_APPOINTMENT_FAILED)
                .setEventName("Create Appointment : Failed")
                .build()
                .post();
    }

    public static void postQueryTechnicianListSuccess( List<User> aTechnicianList ) {
        new IEvent.Builder()
                .setEventID(IUser.GET_TECHNICIAN_LIST_SUCCESS)
                .setEventObject(aTechnicianList)
                .setEventName("Query Technician List Success!")
                .build()
                .post();
    }

    public static void postQueryTechnicianListFailed( String aErrorMessage ) {
        new IEvent.Builder()
                .setEventID(IUser.GET_TECHNICIAN_LIST_FAILED)
                .setEventObject(aErrorMessage)
                .setEventName("Query Technician List Failed!")
                .build()
                .post();
    }

    // Appointments

    // 1. Current user appointments
    public static void postGetCurrentUserAppointment() {
        new IServiceEvent.Builder()
                .setEventID(IAppointment.GET_USER_APPOINTMENTS)
                .setEventName("Get User Appointments from FB Remote")
                .build()
                .post();
    }

    public static void postGetCurrentUserAppointmentSuccess( List<Appointment> aAppointmentList ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_USER_APPOINTMENTS_SUCCESS)
                .setEventObject(aAppointmentList)
                .setEventName("Sending List of Current User Appointment")
                .build()
                .post();
    }

    public static void postGetCurrentUserAppointmentFailed( String aMsg ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_USER_APPOINTMENTS_FAILED)
                .setEventObject(aMsg)
                .setEventName("Failed To Get Current User Appointment")
                .build()
                .post();
    }

    // 2. Get all appointments
    public static void postGetAppointmentSuccess( List<Appointment> aAppointmentList ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_ALL_APPOINTMENTS_SUCCESS)
                .setEventObject(aAppointmentList)
                .setEventName("Sending List of Appointments")
                .build()
                .post();
    }

    public static void postGetAppointmentFailed( String aMsg ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_ALL_APPOINTMENTS_FAILED)
                .setEventObject(aMsg)
                .setEventName("Failed To get Appointment List")
                .build()
                .post();
    }

    // 3. Get appointment by ID
    public static void postGetAppointmentByIDSuccess( Appointment aAppointment ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_APPOINTMENT_BY_ID_SUCCESS)
                .setEventObject(aAppointment)
                .setEventName("Sending Appointment queried by ID")
                .build()
                .post();
    }

    public static void postGetAppointmentByIDFailed( String aMsg ) {

        new IEvent.Builder()
                .setEventID(IAppointment.GET_APPOINTMENT_BY_ID_FAILED)
                .setEventObject(aMsg)
                .setEventName("Failed To get Appointment By ID")
                .build()
                .post();
    }

    // 4. Assign Technician
    public static void postAssignTechnicianEvent( Appointment appointment ) {

        new IServiceEvent.Builder()
                .setEventID(IAppointment.ASSIGN_APPOINTMENT)
                .setEventObject(appointment)
                .setEventName("Assigning Appointment with technician")
                .build()
                .post();
    }

    public static void postAssignTechnicianSuccess( String aMsg ) {

        new IEvent.Builder()
                .setEventID(IAppointment.ASSIGN_APPOINTMENT_SUCCESS)
                .setEventObject(aMsg)
                .setEventName("Assign Appointment with technician : Success")
                .build()
                .post();
    }

    public static void postAssignTechnicianFailed( Exception aException ) {

        new IEvent.Builder()
                .setEventID(IAppointment.ASSIGN_APPOINTMENT_FAILED)
                .setEventObject(aException)
                .setEventName("Assign Appointment with technician : Failed")
                .build()
                .post();
    }

    public static void postStartTaskEvent( Appointment appointment ) {

        new IServiceEvent.Builder()
                .setEventID(IAppointment.START_APPOINTMENT)
                .setEventObject(appointment)
                .setEventName("Starting Appointment")
                .build()
                .post();

    }

    public static void postStartTaskSuccessEvent() {

        new IEvent.Builder()
                .setEventID(IAppointment.START_APPOINTMENT_SUCCESS)
                .setEventName("Task started successfully!")
                .build()
                .post();

    }

    public static void postStartTaskFailedEvent( Exception aException ) {

        new IEvent.Builder()
                .setEventID(IAppointment.START_APPOINTMENT_FAILED)
                .setEventObject(aException)
                .setEventName("Failed to start Task!")
                .build()
                .post();

    }

    public static void postFinishTaskEvent( Appointment appointment ) {

        new IServiceEvent.Builder()
                .setEventID(IAppointment.FINISH_APPOINTMENT)
                .setEventObject(appointment)
                .setEventName("Closing appointment")
                .build()
                .post();

    }

    public static void postFinishTaskSuccessEvent() {

        new IEvent.Builder()
                .setEventID(IAppointment.FINISH_APPOINTMENT_SUCCESS)
                .setEventName("Task finished successfully!")
                .build()
                .post();

    }

    public static void postFinishTaskFailedEvent( Exception aException ) {

        new IEvent.Builder()
                .setEventID(IAppointment.FINISH_APPOINTMENT_FAILED)
                .setEventObject(aException)
                .setEventName("Failed to finish Task!")
                .build()
                .post();
    }

    public static void postAppointmentUpdateEvent( Appointment appointment ) {
        new IEvent.Builder()
                .setEventID(IAppointment.CURRENT_APPOINTMENT_DATA_CHANGED)
                .setEventObject(appointment)
                .setEventName("Appointment Info changed.,")
                .build()
                .post();
    }

    public static void registerSingleValueListener( String appointmentID ) {
        new IServiceEvent.Builder()
                .setEventID(IAppointment.ADD_SINGLE_VALUE_LISTENER)
                .setEventObject(appointmentID)
                .setEventName("Adding listener for the appointment : " + appointmentID)
                .build()
                .post();
    }

    public static void unRegisterSingleValueListener( String appointmentID ) {
        new IServiceEvent.Builder()
                .setEventID(IAppointment.REMOVE_SINGLE_VALUE_LISTENER)
                .setEventObject(appointmentID)
                .setEventName("Removing listener for the appointment : " + appointmentID)
                .build()
                .post();
    }

    public static void registerAllValueListener() {
        new IServiceEvent.Builder()
                .setEventID(IAppointment.ADD_ALL_VALUE_LISTENER)
                .setEventName("Adding listener for the all appointment")
                .build()
                .post();
    }

    public static void unRegisterAllValueListener() {
        new IServiceEvent.Builder()
                .setEventID(IAppointment.REMOVE_ALL_VALUE_LISTENER)
                .setEventName("Removing listener for the all appointment")
                .build()
                .post();
    }
}
